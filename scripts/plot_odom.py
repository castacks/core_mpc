import rosbag
import sys
import numpy as np
import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def quat2Euler(qx, qy, qz, qw):
    roll = math.atan2(2.0 * (qw * qx + qy * qz), 1.0 - 2.0 * (qx * qx + qy * qy))
    pitch = math.asin(2.0 * (qw * qy - qz * qx))
    yaw = math.atan2(2.0 * (qw * qz + qx * qy), 1.0 - 2.0 * (qy * qy + qz * qz))
    return [roll, pitch, yaw]


odom_topic = sys.argv[2]
ref_traj_topic = sys.argv[3]
command_topic = sys.argv[4]

odom_x = []
odom_y = []
odom_z = []
odom_roll = []
odom_pitch = []
odom_yaw = []
odom_vx = []
odom_vy = []
odom_vz = []
odom_t = []

ref_x = []
ref_y = []
ref_z = []
ref_vx = []
ref_vy = []
ref_vz = []
ref_t = []

cmd_roll = []
cmd_pitch = []
cmd_yawrate = []
cmd_thrust = []
cmd_t = []

bag = rosbag.Bag(sys.argv[1])
for topic, msg, t in bag.read_messages(topics=[odom_topic]):
    odom_x.append(msg.pose.pose.position.x)
    odom_y.append(msg.pose.pose.position.y)
    odom_z.append(msg.pose.pose.position.z)

    qx = msg.pose.pose.orientation.x
    qy = msg.pose.pose.orientation.y
    qz = msg.pose.pose.orientation.z
    qw = msg.pose.pose.orientation.w
    euler_angles = quat2Euler(qx, qy, qz, qw)
    odom_roll.append(euler_angles[0])
    odom_pitch.append(euler_angles[1])
    odom_yaw.append(euler_angles[2])

    odom_vx.append(msg.twist.twist.linear.x)
    odom_vy.append(msg.twist.twist.linear.y)
    odom_vz.append(msg.twist.twist.linear.z)
    odom_t.append(t.to_time())

for topic, msg, t in bag.read_messages(topics=[ref_traj_topic]):
    ref_x.append(msg.waypoints[0].position.x)
    ref_y.append(msg.waypoints[0].position.y)
    ref_z.append(msg.waypoints[0].position.z)
    # ref_vx.append(msg.waypoints[0].velocity.linear.x)
    # ref_vy.append(msg.waypoints[0].velocity.linear.y)
    # ref_vz.append(msg.waypoints[0].velocity.linear.z)
    ref_t.append(t.to_time())

for topic, msg, t in bag.read_messages(topics=[command_topic]):
    cmd_roll.append(msg.roll)
    cmd_pitch.append(msg.pitch)
    cmd_yawrate.append(msg.yaw_rate)
    cmd_thrust.append(msg.thrust.z)
    cmd_t.append(t.to_time())

bag.close()

error_x = []
error_y = []
error_z = []
error_t = []

j_idx = 0
for i in range(len(ref_t)):
    for j in range(j_idx, len(odom_t)):
        if odom_t[j] == ref_t[i]:
            error_x.append(np.abs(odom_x[j] - ref_x[i]))
            error_y.append(np.abs(odom_y[j] - ref_y[i]))
            error_z.append(np.abs(odom_z[j] - ref_z[i]))
            error_t.append(odom_t[j])
            j_idx = j+1
            break

print("error_x", np.mean(np.array(error_x)))
print("error_y", np.mean(np.array(error_y)))
print("error_z", np.mean(np.array(error_z)))

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.set_xlim3d(-6, 6)
ax.set_ylim3d(-6, 6)
ax.set_zlim3d(0, 5)
ax.plot(odom_x, odom_y, odom_z, label='odometry')
ax.plot(ref_x, ref_y, ref_z, label='reference')
ax.legend()

fig = plt.figure()
plt.plot(odom_t, odom_x, label='odometry')
plt.plot(ref_t, ref_x, label='reference')
plt.xlabel('time steps')
plt.ylabel('x')
plt.legend()
plt.tight_layout()
# plt.savefig('odom_x.png', bbox_inches='tight')

fig = plt.figure()
plt.plot(odom_t, odom_y, label='odometry')
plt.plot(ref_t, ref_y, label='reference')
plt.xlabel('time steps')
plt.ylabel('y')
plt.legend()
plt.tight_layout()

fig = plt.figure()
plt.plot(odom_t, odom_z, label='odom_z')
plt.plot(ref_t, ref_z, label='ref_z')
plt.legend()
plt.tight_layout()

fig = plt.figure()
plt.plot(error_t, error_x, label='error_x')
plt.legend()
plt.tight_layout()

fig = plt.figure()
plt.plot(error_t, error_y, label='error_y')
plt.legend()
plt.tight_layout()

fig = plt.figure()
plt.plot(error_t, error_z, label='error_z')
plt.legend()
plt.tight_layout()

# fig = plt.figure()
# plt.plot(odom_t, odom_roll, label='odom_roll')
# plt.plot(cmd_t, cmd_roll, label='cmd_roll')
# plt.legend()
# plt.tight_layout()

# fig = plt.figure()
# plt.plot(odom_t, odom_pitch, label='odom_pitch')
# plt.plot(cmd_t, cmd_pitch, label='cmd_pitch')
# plt.legend()
# plt.tight_layout()

# fig = plt.figure()
# plt.plot(odom_t, odom_vx, label='odom_vx')
# # plt.plot(ref_t, ref_vx, label='ref_vx')
# plt.legend()
# plt.tight_layout()

# fig = plt.figure()
# plt.plot(odom_t, odom_vy, label='odom_vy')
# # plt.plot(ref_t, ref_vy, label='ref_vy')
# plt.legend()
# plt.tight_layout()

# fig = plt.figure()
# plt.plot(odom_t, odom_vz, label='odom_vz')
# # plt.plot(ref_t, ref_vz, label='ref_vz')
# plt.legend()
# plt.tight_layout()

plt.show()