import rosbag
import sys
import numpy as np
import math
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

def quat2Euler(qx, qy, qz, qw):
    roll = math.atan2(2.0 * (qw * qx + qy * qz), 1.0 - 2.0 * (qx * qx + qy * qy))
    pitch = math.asin(2.0 * (qw * qy - qz * qx))
    yaw = math.atan2(2.0 * (qw * qz + qx * qy), 1.0 - 2.0 * (qy * qy + qz * qz))
    return [roll, pitch, yaw]

def nominal_cmd(f):
    return 0.02693*f + 0.15


motor_constant = 8.54858e-06
cmd_topic = '/uav1/roll_pitch_yawrate_thrust_command'
vel_topic = '/motors'
odom_topic = '/uav1/odometry'


force = []
cmd = []
attitude = []

bag = rosbag.Bag(sys.argv[1])

for topic, msg, t in bag.read_messages(topics=[cmd_topic]):
    cmd.append([msg.roll, msg.pitch, msg.thrust.z, t.to_time(), t.to_sec(), t.to_nsec()])

for topic, msg, t in bag.read_messages(topics=[odom_topic]):
    qx = msg.pose.pose.orientation.x
    qy = msg.pose.pose.orientation.y
    qz = msg.pose.pose.orientation.z
    qw = msg.pose.pose.orientation.w
    roll, pitch, _ = quat2Euler(qx, qy, qz, qw)
    attitude.append([roll, pitch, t.to_time(), t.to_sec(), t.to_nsec()])

for topic, msg, t in bag.read_messages(topics=[vel_topic]):
    u1 = msg.motor_speed[0]
    u2 = msg.motor_speed[1]
    u3 = msg.motor_speed[2]
    u4 = msg.motor_speed[3]
    f = motor_constant * (u1**2 + u2**2 + u3**2 + u4**2)
    force.append([f, t.to_time(), t.to_sec(), t.to_nsec()])

bag.close()

cmd_iter = 0
C = []
F = []
R = []
P = []
T = []

f_idx = 0
cmd_force = []
prev_t1 = -1
for i in range(len(cmd)):
    # r = np.abs(cmd[i][0])
    # p = np.abs(cmd[i][1])
    c = cmd[i][2]
    t1 = cmd[i][3]
    if t1 == prev_t1:
        continue
    for j in range(f_idx, len(force)):
        f = force[j][0]
        t2 = force[j][1]
        if t1 == t2:
            cmd_force.append([c, f, t1])
            f_idx = j+1
            prev_t1 = t1
            break

print(len(cmd_force))

f_idx = 0
for j in range(len(cmd_force)):
    c = cmd_force[j][0]
    f = cmd_force[j][1]
    t2 = cmd_force[j][2]
    for i in range(f_idx, len(attitude)):
        r = np.abs(attitude[i][0])
        p = np.abs(attitude[i][1])
        t1 = attitude[i][2]
        if t1 == t2:
            C.append(c)
            F.append(nominal_cmd(f))
            R.append(r)
            P.append(p)
            f_idx = i+1
            break

print(len(C), len(F), len(R), len(P), len(T))

X = np.vstack((np.asarray(F), np.asarray(R), np.asarray(P))).T
y = np.asarray(C)
reg = LinearRegression(fit_intercept=False).fit(X, y)
print("Coeff", reg.coef_)
print("Score", reg.score(X, y))


# thrust_map = np.polyfit(F, C, 2)
# print(thrust_map)

# fig, ax1 = plt.subplots()

# color = 'tab:red'
# ax1.set_xlabel('time (s)')
# ax1.set_ylabel('Force (N)', color=color)
# ax1.plot(T, F, color=color)
# ax1.tick_params(axis='y', labelcolor=color)

# ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

# color = 'tab:blue'
# ax2.set_ylabel('Force (0..1)', color=color)  # we already handled the x-label with ax1
# ax2.plot(T, C, color=color)
# ax2.tick_params(axis='y', labelcolor=color)

# plt.figure()
# mapped_C = []
# for f in F:
#     mapped_C.append(thrust_map[0] * (f*f) + thrust_map[1] * f + thrust_map[2])
# plt.plot(T, C, color='r', label='px4')
# plt.plot(T, mapped_C, color='b', label='newton_to_px4')
# plt.legend()

# fig.tight_layout()  # otherwise the right y-label is slightly clipped
# plt.show()