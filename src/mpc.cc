#include <mpc/mpc.h>

// Public functions

MPC::MPC(const ros::NodeHandle& nh)
    : nh_(nh),
      received_first_odometry_(false),
      position_error_integration_(0, 0, 0),
      mpc_queue_(nh, ACADO_N + 1) {
  trajectory_reference_vis_publisher_ =
      nh_.advertise<nav_msgs::Path>("mpc/reference_traj", 1);
  publish_reference_vis_timer_ = nh_.createTimer(
      ros::Duration(0.1), &MPC::PublishReferenceVisualization, this);

  acado_initializeSolver();

  W_.setZero();
  WN_.setZero();

  input_.setZero();
  state_.setZero();
  reference_.setZero();
  referenceN_.setZero();

  predicted_path_pub_ =
      nh_.advertise<nav_msgs::Path>("mpc/trajectory_predicted", 1);
}

void MPC::SetMPCParameters(mpc::ParamsConfig& config) {
  sampling_time_ = config.sampling_time;
  prediction_sampling_time_ = config.prediction_sampling_time;
  mpc_queue_.initializeQueue(sampling_time_, prediction_sampling_time_);

  enable_integrator_ = config.enable_integrator;
  Ki_altitude_ = config.Ki_altitude;
  Ki_xy_ = config.Ki_xy;
  antiwindup_ball_ = config.antiwindup_ball;
  position_error_integration_limit_ = config.position_error_integration_limit;

  q_position_ << config.q_x, config.q_y, config.q_z;
  q_attitude_ << config.q_roll, config.q_pitch, config.q_yaw;
  q_velocity_ << config.q_vx, config.q_vy, config.q_vz;
  r_command_ << config.r_roll, config.r_pitch, config.r_yawrate,
      config.r_thrust;

  W_.block(0, 0, 3, 3) = q_position_.asDiagonal();
  W_.block(3, 3, 3, 3) = q_velocity_.asDiagonal();
  W_.block(6, 6, 3, 3) = q_attitude_.asDiagonal();
  W_.block(9, 9, 4, 4) = r_command_.asDiagonal();

  // WN_ = W_.block(0, 0, 6, 6);
  WN_ = solveCARE(
      (Eigen::VectorXd(6) << q_position_, q_velocity_).finished().asDiagonal(),
      (Eigen::VectorXd(3) << config.r_roll, config.r_pitch, config.r_thrust)
          .finished()
          .asDiagonal());

  Eigen::Map<Eigen::Matrix<double, ACADO_NY, ACADO_NY>>(
      const_cast<double*>(acadoVariables.W)) = W_.transpose();
  Eigen::Map<Eigen::Matrix<double, ACADO_NYN, ACADO_NYN>>(
      const_cast<double*>(acadoVariables.WN)) = WN_.transpose();

  roll_limit_ = config.roll_max;
  pitch_limit_ = config.pitch_max;
  yaw_rate_limit_ = config.yaw_rate_max;
  thrust_min_ = config.thrust_min;
  thrust_max_ = config.thrust_max;

  mass_ = config.mass;
  roll_tau_ = config.roll_tau;
  roll_gain_ = config.roll_gain;
  pitch_tau_ = config.pitch_tau;
  pitch_gain_ = config.pitch_gain;
  drag_coeff_x_ = config.drag_coeff_x;
  drag_coeff_y_ = config.drag_coeff_y;

  for (size_t i = 0; i < ACADO_N; ++i) {
    acadoVariables.lbValues[4 * i] = -roll_limit_;
    acadoVariables.lbValues[4 * i + 1] = -pitch_limit_;
    acadoVariables.lbValues[4 * i + 2] = -yaw_rate_limit_;
    acadoVariables.lbValues[4 * i + 3] = thrust_min_;
    acadoVariables.ubValues[4 * i] = roll_limit_;
    acadoVariables.ubValues[4 * i + 1] = pitch_limit_;
    acadoVariables.ubValues[4 * i + 2] = yaw_rate_limit_;
    acadoVariables.ubValues[4 * i + 3] = thrust_max_;
  }

  for (int i = 0; i < ACADO_N + 1; i++) {
    acado_online_data_.block(i, 0, 1, ACADO_NOD) << roll_tau_, roll_gain_,
        pitch_tau_, pitch_gain_, drag_coeff_x_, drag_coeff_y_;
  }

  Eigen::Map<Eigen::Matrix<double, ACADO_NOD, ACADO_N + 1>>(
      const_cast<double*>(acadoVariables.od)) = acado_online_data_.transpose();

  std::cout << "MPC Params set!" << std::endl;
  std::cout << "W = \n" << W_ << std::endl;
  std::cout << "WN_ = \n" << WN_ << std::endl;
  std::cout << "acado online data: " << std::endl
            << roll_tau_ << " " << roll_gain_ << " " << pitch_tau_ << " "
            << pitch_gain_ << " " << drag_coeff_x_ << " " << drag_coeff_y_
            << std::endl;
}

MPC::~MPC() {}

void MPC::SetOdometry(const mav_msgs::EigenOdometry& odometry) {
  if (!received_first_odometry_) {
    Eigen::Vector3d euler_angles;
    odometry.getEulerAngles(&euler_angles);

    Eigen::VectorXd x0(ACADO_NX);

    x0 << odometry.position_W, euler_angles, odometry.velocity_B;

    InitializeAcadoSolver(x0);
    received_first_odometry_ = true;
  }
  odometry_.position_W = odometry.position_W;
  odometry_.velocity_B = odometry.velocity_B;
  odometry_.angular_velocity_B = odometry.angular_velocity_B;
  odometry_.orientation_W_B = odometry.orientation_W_B;
  odometry_.timestamp_ns = odometry.timestamp_ns;
}

void MPC::SetReferencePose(core_trajectory_msgs::WaypointXYZVYaw& msg) {
  mav_msgs::EigenTrajectoryPoint ref;
  ref.setFromYaw(msg.yaw);
  ref.position_W.x() = msg.position.x;
  ref.position_W.y() = msg.position.y;
  ref.position_W.z() = msg.position.z;
  mpc_queue_.insertReference(ref);
}

void MPC::SetReferencePose(
    const core_trajectory_msgs::WaypointXYZVYawConstPtr& msg) {
  mav_msgs::EigenTrajectoryPoint ref;
  ref.setFromYaw(msg->yaw);
  ref.position_W.x() = msg->position.x;
  ref.position_W.y() = msg->position.y;
  ref.position_W.z() = msg->position.z;
  mpc_queue_.insertReference(ref);
}

void MPC::SetReferenceTrajectory(
    const core_trajectory_msgs::TrajectoryXYZVYawConstPtr& msg) {
  mav_msgs::EigenTrajectoryPointDeque trajectory;
  mav_msgs::EigenTrajectoryPoint point;
  for (int i = 0; i < msg->waypoints.size(); i++) {
    point.setFromYaw(msg->waypoints[i].yaw);
    point.position_W.x() = msg->waypoints[i].position.x;
    point.position_W.y() = msg->waypoints[i].position.y;
    point.position_W.z() = msg->waypoints[i].position.z;
    trajectory.push_back(point);
  }
  mpc_queue_.insertReferenceTrajectory(trajectory);
}

void MPC::SetReferenceTrajectory(
    const trajectory_msgs::MultiDOFJointTrajectoryConstPtr& msg) {
  mav_msgs::EigenTrajectoryPointDeque trajectory;
  mav_msgs::eigenTrajectoryPointDequeFromMsg(*msg, &trajectory);
  mpc_queue_.insertReferenceTrajectory(trajectory);
}

void MPC::SetReferencePose(
    const trajectory_msgs::MultiDOFJointTrajectoryPointConstPtr& msg) {
  mav_msgs::EigenTrajectoryPoint ref;
  mav_msgs::eigenTrajectoryPointFromMsg(*msg, &ref);
  mpc_queue_.insertReference(ref);
}

void MPC::Solve(Eigen::Vector4d* control_ref) {
  Eigen::Matrix<double, ACADO_NX, 1> x_0;
  Eigen::Vector3d estimated_error;

  Eigen::Vector3d current_rpy;
  odometry_.getEulerAngles(&current_rpy);

  mpc_queue_.updateQueue();
  mpc_queue_.getQueue(position_ref_, velocity_ref_, acceleration_ref_, yaw_ref_,
                      yaw_rate_ref_);

  x_0 << odometry_.position_W, current_rpy, odometry_.velocity_B;

  if (enable_integrator_) {
    Eigen::Vector3d position_error =
        position_ref_.front() - odometry_.position_W;
    if (position_error.norm() < antiwindup_ball_) {
      position_error_integration_ += position_error * sampling_time_;
    } else {
      position_error_integration_.setZero();
    }

    position_error_integration_ = position_error_integration_.cwiseMax(
        Eigen::Vector3d(-position_error_integration_limit_,
                        -position_error_integration_limit_,
                        -position_error_integration_limit_));

    position_error_integration_ = position_error_integration_.cwiseMin(
        Eigen::Vector3d(position_error_integration_limit_,
                        position_error_integration_limit_,
                        position_error_integration_limit_));

    estimated_error -=
        Eigen::Vector3d(Ki_xy_, Ki_xy_, Ki_altitude_).asDiagonal() *
        position_error_integration_;
  }

  std::cout << "Estimated error: " << estimated_error.norm() << std::endl;

  Eigen::Vector3d estimated_error_B =
      odometry_.orientation_W_B.toRotationMatrix().transpose() *
      estimated_error;

  std::cout << "Queue size: " << position_ref_.size() << std::endl;

  for (size_t i = 0; i < ACADO_N; i++) {
    Eigen::Vector3d acceleration_ref_B =
        odometry_.orientation_W_B.toRotationMatrix().transpose() *
        acceleration_ref_[i];
    Eigen::Vector2d feed_forward(
        (-(acceleration_ref_B(1) - estimated_error_B(1)) / kGravity),
        ((acceleration_ref_B(0) - estimated_error_B(0)) / kGravity));

    reference_.block(i, 0, 1, ACADO_NY) << position_ref_[i].transpose(),
        velocity_ref_[i].transpose(), feed_forward.transpose(), yaw_ref_[i],
        feed_forward.transpose(), yaw_rate_ref_[i],
        acceleration_ref_[i].z();
  }
  referenceN_ << position_ref_[ACADO_N].transpose(),
      velocity_ref_[ACADO_N].transpose();

  Eigen::Map<Eigen::Matrix<double, ACADO_NX, 1>>(
      const_cast<double*>(acadoVariables.x0)) = x_0;
  Eigen::Map<Eigen::Matrix<double, ACADO_NX, ACADO_N + 1>>(
      const_cast<double*>(acadoVariables.x)) = state_.transpose();
  Eigen::Map<Eigen::Matrix<double, ACADO_NY, ACADO_N>>(
      const_cast<double*>(acadoVariables.y)) = reference_.transpose();
  Eigen::Map<Eigen::Matrix<double, ACADO_NYN, 1>>(
      const_cast<double*>(acadoVariables.yN)) = referenceN_.transpose();
  Eigen::Map<Eigen::Matrix<double, ACADO_NOD, ACADO_N + 1>>(
      const_cast<double*>(acadoVariables.od)) = acado_online_data_.transpose();

  acado_preparationStep();

  int acado_status = acado_feedbackStep();
  printf("Real-Time Iteration:  KKT Tolerance = %.3e", acado_getKKT());

  double roll_ref = acadoVariables.u[0];
  double pitch_ref = acadoVariables.u[1];
  double yaw_ref = acadoVariables.u[2];
  double thrust_ref = acadoVariables.u[3] * mass_;

  if (std::isnan(roll_ref) || std::isnan(pitch_ref) || std::isnan(yaw_ref) ||
      std::isnan(thrust_ref) || acado_status != 0) {
    ROS_WARN_STREAM("MPC failed with status: " << acado_status);
    ROS_WARN("Reinitializing...");
    InitializeAcadoSolver(x_0);
    *control_ref << 0, 0, 0, kGravity * mass_;
    return;
  }
  *control_ref << roll_ref, pitch_ref, yaw_ref, thrust_ref;

  state_ =
      Eigen::Map<Eigen::Matrix<double, ACADO_N + 1, ACADO_NX, Eigen::RowMajor>>(
          acadoVariables.x);
  PublishPredictedPath();
}

void MPC::PublishPredictedPath() {
  nav_msgs::Path path_msg;
  path_msg.header.stamp = ros::Time::now();
  path_msg.header.frame_id = "world";
  geometry_msgs::PoseStamped pose;

  for (int i = 0; i < ACADO_N + 1; i++) {
    pose.header.stamp =
        ros::Time::now() + ros::Duration(i * prediction_sampling_time_);
    pose.header.seq = i;
    pose.pose.position.x = state_(i, 0);
    pose.pose.position.y = state_(i, 1);
    pose.pose.position.z = state_(i, 2);
    path_msg.poses.push_back(pose);
  }
  predicted_path_pub_.publish(path_msg);
}

// Private functions

void MPC::InitializeAcadoSolver(Eigen::VectorXd x0) {
  for (int i = 0; i < ACADO_N + 1; i++) {
    state_.block(i, 0, 1, ACADO_NX) << x0.transpose();
  }

  Eigen::Map<Eigen::Matrix<double, ACADO_NX, ACADO_N + 1>>(
      const_cast<double*>(acadoVariables.x)) = state_.transpose();
  Eigen::Map<Eigen::Matrix<double, ACADO_NU, ACADO_N>>(
      const_cast<double*>(acadoVariables.u)) = input_.transpose();
  Eigen::Map<Eigen::Matrix<double, ACADO_NY, ACADO_N>>(
      const_cast<double*>(acadoVariables.y)) = reference_.transpose();
  Eigen::Map<Eigen::Matrix<double, ACADO_NYN, 1>>(
      const_cast<double*>(acadoVariables.yN)) = referenceN_.transpose();
}

Eigen::MatrixXd MPC::solveCARE(Eigen::MatrixXd Q, Eigen::MatrixXd R) {
  // Define system matrices
  Eigen::MatrixXd A;
  A.resize(6, 6);
  A.setZero();

  A.block(0, 3, 3, 3) = Eigen::Matrix3d::Identity();
  A.block(3, 3, 3, 3) =
      -1.0 * Eigen::Vector3d(drag_coeff_x_, drag_coeff_y_, 0).asDiagonal();

  Eigen::MatrixXd B;
  B.resize(6, 3);
  B.setZero();

  B(3, 1) = kGravity;
  B(4, 0) = -1.0 * kGravity;
  B(5, 2) = 1.0;

  Eigen::MatrixXd G = B * R.inverse() * B.transpose();

  Eigen::MatrixXd z11 = A;
  Eigen::MatrixXd z12 = -1.0 * G;
  Eigen::MatrixXd z21 = -1.0 * Q;
  Eigen::MatrixXd z22 = -1.0 * A.transpose();

  Eigen::MatrixXd Z;
  Z.resize(z11.rows() + z21.rows(), z11.cols() + z12.cols());
  Z << z11, z12, z21, z22;

  int n = A.cols();
  Eigen::MatrixXd U(2 * n,
                    2 * n);  // Orthogonal matrix from Schur decomposition
  Eigen::VectorXd WR(2 * n);
  Eigen::VectorXd WI(2 * n);
  lapack_int sdim = 0;  // Number of eigenvalues for which sort is true
  lapack_int info;
  info = LAPACKE_dgees(LAPACK_COL_MAJOR,  // Eigen default storage order
                       'V',               // Schur vectors are computed
                       'S',               // Eigenvalues are sorted
                       select_lhp,        // Ordering callback
                       Z.rows(),          // Dimension of test matrix
                       Z.data(),          // Pointer to first element
                       Z.rows(),          // Leading dimension (column stride)
                       &sdim,             // Number of eigenvalues sort is true
                       WR.data(),         // Real portion of eigenvalues
                       WI.data(),         // Complex portion of eigenvalues
                       U.data(),          // Orthogonal transformation matrix
                       Z.rows());         // Dimension of Z

  Eigen::MatrixXd U11 = U.block(0, 0, n, n).transpose();
  Eigen::MatrixXd U21 = U.block(n, 0, n, n).transpose();

  return U11.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV)
      .solve(U21)
      .transpose();
}

void MPC::PublishReferenceVisualization(const ros::TimerEvent&) {
  if (trajectory_reference_vis_publisher_.getNumSubscribers() > 0) {
    nav_msgs::Path vis_msg;
    vis_msg.header.stamp = ros::Time::now();
    vis_msg.header.frame_id = "world";
    geometry_msgs::PoseStamped vis_pose;

    for (int i = 0; i < position_ref_.size(); i++) {
      vis_pose.header.stamp =
          ros::Time::now() + ros::Duration(i * prediction_sampling_time_);
      vis_pose.header.seq = i;
      vis_pose.pose.position.x = position_ref_[i].x();
      vis_pose.pose.position.y = position_ref_[i].y();
      vis_pose.pose.position.z = position_ref_[i].z();
      vis_msg.poses.push_back(vis_pose);
    }

    trajectory_reference_vis_publisher_.publish(vis_msg);
  }
}