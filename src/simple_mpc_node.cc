#include <mpc/mpc.h>
#include <sensor_msgs/BatteryState.h>

ros::Publisher cmd_pub;

MPC* controller;
mav_msgs::EigenOdometry odometry;
double voltage = 0;
double pitch_correction = 0;
double roll_correction = 0;

double thrust_newton_to_px4(double f) {
  // return -0.00035 * (f * f) + 0.03606908 * f + 0.08797804;
  // return -0.00025633 * (f * f) + 0.03147742 * f + 0.14074639;
  return 0.02693*f + 0.15;
  // return 0.05*f + 0.1;
}

double thrust_newton_to_px4(double f, double r, double p) {
  // return 0.02092705*f + 0.01660002*fabs(r) + 0.03636566*fabs(p) + 0.1860;
  return thrust_newton_to_px4(f) - fabs(p) * pitch_correction - fabs(r) * roll_correction;
}

double thrust_newton_to_px4_real(double f) {
  double pwm = (f - 286.8 + 15.48*voltage)/(-0.2563);
  return (pwm - 1000.0)/1000.0;
}

void PublishCommand(const ros::TimerEvent&) {
  Eigen::Vector4d* control_ref = new Eigen::Vector4d(0, 0, 0, 0);
  controller->Solve(control_ref);
  std::cout << std::endl << "Velocity: " << odometry.getVelocityWorld().norm() << std::endl;
  std::cout << std::endl << "Control: " << *control_ref << std::endl;

  mav_msgs::RollPitchYawrateThrust cmd_msg;
  cmd_msg.roll = control_ref->x();
  cmd_msg.pitch = control_ref->y();
  cmd_msg.yaw_rate = control_ref->z();
  // cmd_msg.thrust.z = control_ref->w();
  // cmd_msg.thrust.z = thrust_newton_to_px4(control_ref->w());
  cmd_msg.thrust.z = thrust_newton_to_px4(control_ref->w(), control_ref->x(), control_ref->y());
  cmd_msg.header.stamp = ros::Time::now();
  cmd_pub.publish(cmd_msg);
}

void RefPoseCallback(const core_trajectory_msgs::WaypointXYZVYawConstPtr& msg) {
  controller->SetReferencePose(msg);
  //PublishCommand();
}

void RefTrajCallback(
    const core_trajectory_msgs::TrajectoryXYZVYawConstPtr& msg) {
  controller->SetReferenceTrajectory(msg);
  // controller->InterpolateReferenceTrajectory(msg);
  //PublishCommand();
}

void RefMavTrajCallback(
    const trajectory_msgs::MultiDOFJointTrajectoryConstPtr& msg) {
  controller->SetReferenceTrajectory(msg); 
}

void RefMavTrajPointCallback(
    const trajectory_msgs::MultiDOFJointTrajectoryPointConstPtr& msg) {
  controller->SetReferencePose(msg);
}

void RefCoreTrajCallback(
    const core_trajectory_msgs::TrajectoryXYZVYawConstPtr& msg) {
  controller->SetReferenceTrajectory(msg);
  // controller->InterpolateReferenceTrajectory(msg);
  // controller->ExtrapolateReferenceTrajectory(msg);
  //PublishCommand();
  // nav_msgs::Path vis_msg;
  // vis_msg.header.stamp = ros::Time::now();
  // vis_msg.header.frame_id = "world";
  // geometry_msgs::PoseStamped vis_pose;

  // for (int i = 0; i < msg->waypoints.size(); i++) {
  //   vis_pose.header.stamp =
  //       ros::Time::now() + ros::Duration(i * 0.1);
  //   vis_pose.header.seq = i;
  //   vis_pose.pose.position.x = msg->waypoints[i].position.x;
  //   vis_pose.pose.position.y = msg->waypoints[i].position.y;
  //   vis_pose.pose.position.z = msg->waypoints[i].position.z;
  //   vis_msg.poses.push_back(vis_pose);
  // }
  // test_pub.publish(vis_msg);
}

void RefTrackingPointCallback(const nav_msgs::Odometry::ConstPtr& msg) {
  core_trajectory_msgs::WaypointXYZVYaw point;
  mav_msgs::EigenOdometry track_odom;
  mav_msgs::eigenOdometryFromMsg(*msg, &track_odom);
  point.position.x = msg->pose.pose.position.x;
  point.position.y = msg->pose.pose.position.y;
  point.position.z = msg->pose.pose.position.z;
  point.yaw = track_odom.getYaw();
  point.velocity = 3.0;
  controller->SetReferencePose(point);
  //PublishCommand();
}


void OdometryCallback(const nav_msgs::Odometry::ConstPtr& msg) {
  mav_msgs::eigenOdometryFromMsg(*msg, &odometry);
  controller->SetOdometry(odometry);
}

void DynParamsCallback(mpc::ParamsConfig& config, uint32_t level) {
  controller->SetMPCParameters(config);
  pitch_correction = config.pitch_correction;
  roll_correction = config.roll_correction;
}

void BatteryCallback(const sensor_msgs::BatteryState::ConstPtr& msg) {
  voltage = msg->voltage;
}


int main(int argc, char** argv) {
  ros::init(argc, argv, "simple_mpc_node");
  ros::NodeHandle n;

  controller = new MPC(n);

  dynamic_reconfigure::Server<mpc::ParamsConfig> server;
  dynamic_reconfigure::Server<mpc::ParamsConfig>::CallbackType f;

  f = boost::bind(&DynParamsCallback, _1, _2);
  server.setCallback(f);

  ros::Subscriber sub_battery =
      n.subscribe("mavros/battery", 1, BatteryCallback);
  ros::Subscriber sub_odom = n.subscribe("odometry", 1, OdometryCallback);
  ros::Subscriber sub_cmd_pose =
      n.subscribe("command/current_setpoint", 1, RefPoseCallback);
  ros::Subscriber sub_cmd_traj =
      n.subscribe("command/current_trajectory", 1, RefTrajCallback);
  ros::Subscriber sub_mav_traj =
      n.subscribe("command/mav_trajectory", 1, RefMavTrajCallback);
  ros::Subscriber sub_mav_traj_pt =
      n.subscribe("command/mav_trajectory_point", 1, RefMavTrajPointCallback);
  // ros::Subscriber sub_core_traj = n.subscribe("trajectory_segment", 1, RefCoreTrajCallback);
  ros::Subscriber sub_tracking_point = n.subscribe("tracking_point", 1, RefTrackingPointCallback);
  cmd_pub = n.advertise<mav_msgs::RollPitchYawrateThrust>(
      "roll_pitch_yawrate_thrust_command", 1);

  ros::Timer mpc_loop =
      n.createTimer(ros::Duration(0.008), PublishCommand);

  ros::spin();
  delete controller;

  return 0;
}
