#include <mavros_msgs/AttitudeTarget.h>
#include <mpc/mpc_rate.h>
#include <sensor_msgs/BatteryState.h>

ros::Publisher cmd_pub;

MPCRate* controller;
mav_msgs::EigenOdometry odometry;
double voltage = 0;
double pitch_correction = 0;
double roll_correction = 0;

double thrust_newton_to_px4(double f) {
  // return -0.00035 * (f * f) + 0.03606908 * f + 0.08797804;
  return -0.00025633 * (f * f) + 0.03147742 * f + 0.14074639;
  // return 0.02693 * f + 0.15;
  // return 0.05*f + 0.1;
}

double thrust_newton_to_px4(double f, double r, double p) {
  // return 0.02092705*f + 0.01660002*fabs(r) + 0.03636566*fabs(p) + 0.1860;
  return thrust_newton_to_px4(f) - fabs(p) * pitch_correction -
         fabs(r) * roll_correction;
}

double thrust_newton_to_px4_real(double f) {
  double pwm = (f - 286.8 + 15.48 * voltage) / (-0.2563);
  return (pwm - 1000.0) / 1000.0;
}

void PublishCommand(const ros::TimerEvent&) {
  Eigen::Vector4d* control_ref = new Eigen::Vector4d(0, 0, 0, 0);
  controller->Solve(control_ref);
  std::cout << std::endl
            << "Velocity: " << odometry.getVelocityWorld().norm() << std::endl;
  std::cout << std::endl << "Control: " << *control_ref << std::endl;

  mavros_msgs::AttitudeTarget att;
  att.header.frame_id = "world";
  att.header.stamp = ros::Time::now();
  att.type_mask = mavros_msgs::AttitudeTarget::IGNORE_ATTITUDE;
  att.body_rate.x = control_ref->x();
  att.body_rate.y = control_ref->y();
  att.body_rate.z = control_ref->z();
  att.thrust = std::min(1.0, thrust_newton_to_px4(control_ref->w()));
  cmd_pub.publish(att);
}

void RefMavTrajCallback(
    const trajectory_msgs::MultiDOFJointTrajectoryConstPtr& msg) {
  controller->SetReferenceTrajectory(msg);
}

void RefMavTrajPointCallback(
    const trajectory_msgs::MultiDOFJointTrajectoryPointConstPtr& msg) {
  controller->SetReferencePose(msg);
}

void OdometryCallback(const nav_msgs::Odometry::ConstPtr& msg) {
  mav_msgs::eigenOdometryFromMsg(*msg, &odometry);
  controller->SetOdometry(odometry);
}

void DynParamsCallback(mpc::RateParamsConfig& config, uint32_t level) {
  controller->SetMPCParameters(config);
}

void BatteryCallback(const sensor_msgs::BatteryState::ConstPtr& msg) {
  voltage = msg->voltage;
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "mpc_rate_node");
  ros::NodeHandle n;

  controller = new MPCRate(n);

  dynamic_reconfigure::Server<mpc::RateParamsConfig> server;
  dynamic_reconfigure::Server<mpc::RateParamsConfig>::CallbackType f;

  f = boost::bind(&DynParamsCallback, _1, _2);
  server.setCallback(f);

  ros::Subscriber sub_battery =
      n.subscribe("mavros/battery", 1, BatteryCallback);
  ros::Subscriber sub_odom = n.subscribe("odometry", 1, OdometryCallback);
  ros::Subscriber sub_mav_traj =
      n.subscribe("command/mav_trajectory", 1, RefMavTrajCallback);
  ros::Subscriber sub_mav_traj_pt =
      n.subscribe("command/mav_trajectory_point", 1, RefMavTrajPointCallback);
  // cmd_pub = n.advertise<mav_msgs::RollPitchYawrateThrust>(
  //     "roll_pitch_yawrate_thrust_command", 1);
  cmd_pub =
      n.advertise<mavros_msgs::AttitudeTarget>("mavros/setpoint_raw/attitude", 1);

  ros::Timer mpc_loop = n.createTimer(ros::Duration(0.008), PublishCommand);

  ros::spin();
  delete controller;

  return 0;
}
