#include <mavros_msgs/AttitudeTarget.h>
#include <mpc/mpc.h>
#include <tf/transform_datatypes.h>

ros::Publisher cmd_pub;
ros::Publisher px4_cmd_pub;

MPC* controller;
mav_msgs::EigenOdometry odometry;

void RefTrajCallback(
    const core_trajectory_msgs::TrajectoryXYZVYawConstPtr& msg) {
  controller->SetReferenceTrajectory(msg);
  Eigen::Vector4d* control_ref = new Eigen::Vector4d(0, 0, 0, 0);
  controller->Solve(control_ref);
  std::cout << std::endl << "Control: " << *control_ref << std::endl;

  mav_msgs::RollPitchYawrateThrust cmd_msg;
  cmd_msg.roll = control_ref->x();
  cmd_msg.pitch = control_ref->y();
  cmd_msg.yaw_rate = control_ref->z();
  cmd_msg.thrust.z = control_ref->w();
  cmd_msg.header.stamp = ros::Time::now();
  cmd_pub.publish(cmd_msg);

  mavros_msgs::AttitudeTarget att;
  // att.header.frame_id = "world";
  att.header.stamp = ros::Time::now();
  att.type_mask = mavros_msgs::AttitudeTarget::IGNORE_ROLL_RATE |
                  mavros_msgs::AttitudeTarget::IGNORE_PITCH_RATE;
  tf::Quaternion q;
  q.setRPY(control_ref->x(), control_ref->y(), odometry.getYaw());
  att.body_rate.z = control_ref->z();
  att.thrust = control_ref->w();

  att.orientation.x = q.x();
  att.orientation.y = q.y();
  att.orientation.z = q.z();
  att.orientation.w = q.w();

  px4_cmd_pub.publish(att);
}

void OdometryCallback(const nav_msgs::Odometry::ConstPtr& msg) {
  mav_msgs::eigenOdometryFromMsg(*msg, &odometry);
  controller->SetOdometry(odometry);
}

void DynParamsCallback(mpc::ParamsConfig& config, uint32_t level) {
  controller->SetMPCParameters(config);
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "core_mpc_node");
  ros::NodeHandle n;

  controller = new MPC(n);

  dynamic_reconfigure::Server<mpc::ParamsConfig> server;
  dynamic_reconfigure::Server<mpc::ParamsConfig>::CallbackType f;

  f = boost::bind(&DynParamsCallback, _1, _2);
  server.setCallback(f);

  ros::Subscriber sub_odom =
      n.subscribe("/mavros/odometry/in", 1, OdometryCallback);
  ros::Subscriber sub_cmd_traj =
      n.subscribe("/command/current_trajectory", 1, RefTrajCallback);
  cmd_pub = n.advertise<mav_msgs::RollPitchYawrateThrust>(
      "/roll_pitch_yawrate_thrust_command", 1);
  px4_cmd_pub = n.advertise<mavros_msgs::AttitudeTarget>(
      "/mavros/setpoint_raw/attitude", 1);

  ros::spin();

  return 0;
}
