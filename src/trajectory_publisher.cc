#include <core_trajectory_msgs/TrajectoryXYZVYaw.h>
#include <core_trajectory_msgs/WaypointXYZVYaw.h>
#include <eigen_conversions/eigen_msg.h>
#include <mav_msgs/common.h>
#include <mav_msgs/conversions.h>
#include <mav_msgs/eigen_mav_msgs.h>
#include <mav_trajectory_generation/polynomial_optimization_nonlinear.h>
#include <mav_trajectory_generation/trajectory_sampling.h>
#include <mav_trajectory_generation_ros/ros_conversions.h>
#include <mav_trajectory_generation_ros/ros_visualization.h>
#include <trajectory_msgs/MultiDOFJointTrajectory.h>
#include <visualization_msgs/MarkerArray.h>
#include <nav_msgs/Path.h>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/String.h>

double mpc_plan_time = 2.0;
static constexpr int ACADO_N = 20;
mav_msgs::EigenOdometry odometry;
ros::Publisher traj_pub;
ros::Publisher mav_traj_pub;
ros::Publisher traj_pub_vis;
ros::Publisher setpoint_pub;
ros::Publisher mav_traj_pt_pub;
ros::Publisher mav_traj_marker_pub;
std::string traj_type;

Eigen::Affine3d current_pose_;
Eigen::Vector3d current_velocity_;
Eigen::Vector3d current_angular_velocity_;
double max_v_ = 3.0;
double max_a_ = 3.0;
double max_ang_v_ = 3.0;
double max_ang_a_ = 2.0;
bool mav_published = false;

void PublishCircularTrajectory(double r, double v) {
  core_trajectory_msgs::TrajectoryXYZVYaw trajectory;
  trajectory.header.stamp = ros::Time::now();

  nav_msgs::Path vis_msg;
  vis_msg.header.stamp = ros::Time::now();
  vis_msg.header.frame_id = "world";
  geometry_msgs::PoseStamped vis_pose;

  double theta = atan2(odometry.position_W.y(), odometry.position_W.x());
  double theta_inc = (mpc_plan_time * v) / (r * (ACADO_N + 1));
  for (int i = 0; i < ACADO_N + 1; i++) {
    core_trajectory_msgs::WaypointXYZVYaw waypoint;
    waypoint.position.x = 0 + r * cos(theta + (double)(i + 1) * theta_inc);
    waypoint.position.y = 0 + r * sin(theta + (double)(i + 1) * theta_inc);
    waypoint.position.z = 3;
    waypoint.velocity = v;
    waypoint.yaw = 0;
    // waypoint.velocity.linear.x =
    //     -r * sin(theta + (double)(i + 1) * theta_inc) * w;
    // waypoint.velocity.linear.y =
    //     r * cos(theta + (double)(i + 1) * theta_inc) * w;
    // waypoint.velocity.linear.z = 0;
    trajectory.waypoints.push_back(waypoint);

    vis_pose.header.stamp = ros::Time::now() + ros::Duration(i * 0.1);
    vis_pose.header.seq = i;
    vis_pose.pose.position.x = waypoint.position.x;
    vis_pose.pose.position.y = waypoint.position.y;
    vis_pose.pose.position.z = waypoint.position.z;
    vis_msg.poses.push_back(vis_pose);
  }
  traj_pub.publish(trajectory);

  // traj_pub_vis.publish(vis_msg);
}

void PublishSetPoint(double x, double y, double z, double v) {
  core_trajectory_msgs::WaypointXYZVYaw setpoint;
  setpoint.position.x = x;
  setpoint.position.y = y;
  setpoint.position.z = z;
  setpoint.velocity = v;
  setpoint_pub.publish(setpoint);
}

void PublishLine(double v, int dir) {
  nav_msgs::Path vis_msg;
  vis_msg.header.stamp = ros::Time::now();
  vis_msg.header.frame_id = "world";
  geometry_msgs::PoseStamped vis_pose;

  core_trajectory_msgs::TrajectoryXYZVYaw trajectory;
  for (int i = 0; i < ACADO_N + 1; i++) {
    core_trajectory_msgs::WaypointXYZVYaw waypoint;
    if (dir == 0) {
      waypoint.position.x = odometry.position_W.x() + (i + 1) * v * 0.1;
      waypoint.position.y = 0.0;
    } else {
      waypoint.position.x = 0;
      waypoint.position.y = odometry.position_W.y() + (i + 1) * v * 0.1;
    }
    waypoint.position.z = 3;
    waypoint.velocity = v;
    trajectory.waypoints.push_back(waypoint);

    vis_pose.header.stamp = ros::Time::now() + ros::Duration(i * 0.1);
    vis_pose.header.seq = i;
    vis_pose.pose.position.x = waypoint.position.x;
    vis_pose.pose.position.y = waypoint.position.y;
    vis_pose.pose.position.z = waypoint.position.z;
    vis_msg.poses.push_back(vis_pose);
  }
  trajectory.header.stamp = ros::Time::now();
  traj_pub.publish(trajectory);

  // traj_pub_vis.publish(vis_msg);
}

void PublishLine(double x, double y, double z, double v) {
  nav_msgs::Path vis_msg;
  vis_msg.header.stamp = ros::Time::now();
  vis_msg.header.frame_id = "world";
  geometry_msgs::PoseStamped vis_pose;

  core_trajectory_msgs::TrajectoryXYZVYaw trajectory;
  Eigen::Vector3d direction(x - odometry.position_W.x(),
                            y - odometry.position_W.y(),
                            z - odometry.position_W.z());
  double t = 1.0 / (double)(ACADO_N + 1);
  for (int i = 0; i < ACADO_N + 1; i++) {
    core_trajectory_msgs::WaypointXYZVYaw waypoint;
    waypoint.position.x = odometry.position_W.x() + (i)*t * direction.x();
    waypoint.position.y = odometry.position_W.y() + (i)*t * direction.y();
    waypoint.position.z = odometry.position_W.z() + (i)*t * direction.z();
    waypoint.velocity = v;
    trajectory.waypoints.push_back(waypoint);

    vis_pose.header.stamp = ros::Time::now() + ros::Duration(i * 0.1);
    vis_pose.header.seq = i;
    vis_pose.pose.position.x = waypoint.position.x;
    vis_pose.pose.position.y = waypoint.position.y;
    vis_pose.pose.position.z = waypoint.position.z;
    vis_msg.poses.push_back(vis_pose);
  }
  trajectory.header.stamp = ros::Time::now();
  traj_pub.publish(trajectory);

  // traj_pub_vis.publish(vis_msg);
}

bool planCircularTrajectory(double r, double v, mav_trajectory_generation::Trajectory* trajectory) {
  double theta = atan2(odometry.position_W.y(), odometry.position_W.x());
  double theta_inc = (mpc_plan_time * v) / (r * (ACADO_N + 1));

  // 3 Dimensional trajectory => through carteisan space, no orientation
  const int dimension = 3;

  // Array for all waypoints and their constrains
  mav_trajectory_generation::Vertex::Vector vertices;

  // Optimze up to 4th order derivative (SNAP)
  const int derivative_to_optimize =
      mav_trajectory_generation::derivative_order::SNAP;

  mav_trajectory_generation::Vertex start(dimension), end(dimension);

  /******* Configure start point *******/
  // set start point constraints to current position and set all derivatives to
  // zero
  start.makeStartOrEnd(current_pose_.translation(), derivative_to_optimize);

  // set start point's velocity to be constrained to current velocity
  // start.addConstraint(mav_trajectory_generation::derivative_order::VELOCITY,
  //                     current_velocity_);

  // add waypoint to list
  vertices.push_back(start);

  double w = v / r;

  for (int i = 0; i < ACADO_N; i += 5) {
    Eigen::Vector3d pos(r * cos(theta + (double)(i + 1) * theta_inc), r * sin(theta + (double)(i + 1) * theta_inc), 3);
    Eigen::Vector3d vel(-r * w * sin(theta + (double)(i + 1) * theta_inc), r * w * cos(theta + (double)(i + 1) * theta_inc), 0);
    mav_trajectory_generation::Vertex intermediate(dimension);
    intermediate.addConstraint(mav_trajectory_generation::derivative_order::POSITION, pos);
    // intermediate.addConstraint(mav_trajectory_generation::derivative_order::VELOCITY, vel);
    vertices.push_back(intermediate);
  }

  /******* Configure end point *******/
  // set end point constraints to desired position and set all derivatives to
  // zero

  Eigen::Vector3d goal_pos(r * cos(theta + (double)(ACADO_N + 1) * theta_inc), r * sin(theta + (double)(ACADO_N + 1) * theta_inc), 3);
  // Eigen::Vector3d goal_vel(-r * w * sin(theta + (double)(ACADO_N + 1) * theta_inc), r * w * cos(theta + (double)(ACADO_N + 1) * theta_inc), 0);
  end.makeStartOrEnd(goal_pos, derivative_to_optimize);

  // set start point's velocity to be constrained to current velocity
  // end.addConstraint(mav_trajectory_generation::derivative_order::VELOCITY,
  //                   goal_vel);

  // add waypoint to list
  vertices.push_back(end);

  // estimate initial segment times
  std::vector<double> segment_times;
  segment_times = estimateSegmentTimes(vertices, max_v_, max_a_);

  // Set up polynomial solver with default params
  mav_trajectory_generation::NonlinearOptimizationParameters parameters;

  // set up optimization problem
  const int N = 10;
  mav_trajectory_generation::PolynomialOptimizationNonLinear<N> opt(dimension,
                                                                    parameters);
  opt.setupFromVertices(vertices, segment_times, derivative_to_optimize);

  // constrain velocity and acceleration
  opt.addMaximumMagnitudeConstraint(
      mav_trajectory_generation::derivative_order::VELOCITY, max_v_);
  opt.addMaximumMagnitudeConstraint(
    mav_trajectory_generation::derivative_order::ANGULAR_VELOCITY, max_ang_v_);
  opt.addMaximumMagnitudeConstraint(
      mav_trajectory_generation::derivative_order::ACCELERATION, max_a_);
      
  // solve trajectory
  opt.optimize();

  // get trajectory as polynomial parameters
  opt.getTrajectory(&(*trajectory));

  return true;
}

// Plans a trajectory from the current position to the a goal position and
// velocity we neglect attitude here for simplicity
bool planTrajectory(const Eigen::VectorXd& goal_pos,
                    const Eigen::VectorXd& goal_vel,
                    mav_trajectory_generation::Trajectory* trajectory) {
  // 3 Dimensional trajectory => through carteisan space, no orientation
  const int dimension = 3;

  // Array for all waypoints and their constrains
  mav_trajectory_generation::Vertex::Vector vertices;

  // Optimze up to 4th order derivative (SNAP)
  const int derivative_to_optimize =
      mav_trajectory_generation::derivative_order::SNAP;

  // we have 2 vertices:
  // Start = current position
  // end = desired position and velocity
  mav_trajectory_generation::Vertex start(dimension), end(dimension);

  /******* Configure start point *******/
  // set start point constraints to current position and set all derivatives to
  // zero
  start.makeStartOrEnd(current_pose_.translation(), derivative_to_optimize);

  // set start point's velocity to be constrained to current velocity
  // start.addConstraint(mav_trajectory_generation::derivative_order::VELOCITY,
  //                     current_velocity_);

  // add waypoint to list
  vertices.push_back(start);

  mav_trajectory_generation::Vertex middle(dimension);
  middle.addConstraint(mav_trajectory_generation::derivative_order::POSITION, (current_pose_.translation() + goal_pos)/2.0);
  vertices.push_back(middle);

  /******* Configure end point *******/
  // set end point constraints to desired position and set all derivatives to
  // zero
  end.makeStartOrEnd(goal_pos, derivative_to_optimize);

  // set start point's velocity to be constrained to current velocity
  end.addConstraint(mav_trajectory_generation::derivative_order::VELOCITY,
                    goal_vel);

  // add waypoint to list
  vertices.push_back(end);

  // estimate initial segment times
  std::vector<double> segment_times;
  segment_times = estimateSegmentTimes(vertices, max_v_, max_a_);

  // Set up polynomial solver with default params
  mav_trajectory_generation::NonlinearOptimizationParameters parameters;

  // set up optimization problem
  const int N = 10;
  mav_trajectory_generation::PolynomialOptimizationNonLinear<N> opt(dimension,
                                                                    parameters);
  opt.setupFromVertices(vertices, segment_times, derivative_to_optimize);

  // constrain velocity and acceleration
  opt.addMaximumMagnitudeConstraint(
      mav_trajectory_generation::derivative_order::VELOCITY, max_v_);
  opt.addMaximumMagnitudeConstraint(
    mav_trajectory_generation::derivative_order::ANGULAR_VELOCITY, max_ang_v_);
  opt.addMaximumMagnitudeConstraint(
      mav_trajectory_generation::derivative_order::ACCELERATION, max_a_);
      
  // solve trajectory
  opt.optimize();

  // get trajectory as polynomial parameters
  opt.getTrajectory(&(*trajectory));

  return true;
}

void OdometryCallback(const nav_msgs::Odometry::ConstPtr& msg) {
  mav_msgs::eigenOdometryFromMsg(*msg, &odometry);

  // store current position in our planner
  tf::poseMsgToEigen(msg->pose.pose, current_pose_);

  // store current velocity
  tf::vectorMsgToEigen(msg->twist.twist.linear, current_velocity_);
  tf::vectorMsgToEigen(msg->twist.twist.angular, current_angular_velocity_);

  // if (!traj_type.empty()) {
  //   std::stringstream ss(traj_type);
  //   char t;
  //   ss >> t;
  //   if (t == 'p') {
  //     double x, y, z, v;
  //     ss >> x >> y >> z >> v;
  //     PublishSetPoint(x, y, z, v);
  //   } else if (t == 'c') {
  //     double r, v;
  //     ss >> r >> v;
  //     PublishCircularTrajectory(r, v);
  //   } else if (t == 'l') {
  //     double x, y, z, v;
  //     ss >> x >> y >> z >> v;
  //     PublishLine(x, y, z, v);
  //   } else if (t == 'g') {
  //     double v;
  //     ss >> v;
  //     PublishLine(v, 0);
  //   } else if (t == 'h') {
  //     double v;
  //     ss >> v;
  //     PublishLine(v, 1);
  //   } else if (t == 'm' && !mav_published) {
  //     double x, y, z;
  //     ss >> x >> y >> z;
  //     // define set point
  //     Eigen::Vector3d position, velocity;
  //     position << x, y, z;
  //     velocity << 0.0, 0.0, 0.0;

  //     double distance_to_goal = (odometry.position_W - position).norm();
  //     std::cout << "Distance to goal: " << distance_to_goal << std::endl;
  //     if (distance_to_goal < 0.2) {
  //       std::cout << "Published point reference" << std::endl;
  //       mav_msgs::EigenTrajectoryPoint ref;
  //       ref.position_W = position;
  //       ref.velocity_W << 0, 0, 0;
  //       trajectory_msgs::MultiDOFJointTrajectoryPoint ref_msg;
  //       mav_msgs::msgMultiDofJointTrajectoryPointFromEigen(ref, &ref_msg);
  //       mav_traj_pt_pub.publish(ref_msg);
  //       return;
  //     }

  //     mav_trajectory_generation::Trajectory trajectory;
  //     if (planTrajectory(position, velocity, &trajectory)) {
  //       mav_msgs::EigenTrajectoryPoint state;
  //       mav_msgs::EigenTrajectoryPoint::Vector states;
  //       visualization_msgs::MarkerArray traj_marker;

  //       // Sample range:
  //       double t_start = 0.0;
  //       double dt = 0.1;
  //       double duration = std::min(mpc_plan_time, trajectory.getMaxTime());
  //       mav_trajectory_generation::sampleTrajectoryInRange(trajectory, t_start, duration, dt, &states);

  //       std::cout << "Total waypoints: " << states.size() << std::endl;

  //       trajectory_msgs::MultiDOFJointTrajectory mav_traj_msg;
  //       mav_msgs::msgMultiDofJointTrajectoryFromEigen(states, &mav_traj_msg);
  //       mav_traj_pub.publish(mav_traj_msg);

  //       mav_trajectory_generation::drawMavTrajectory(trajectory, 0, "world", &traj_marker);
  //       // mav_trajectory_generation::drawMavSampledTrajectorybyTime(states, dt, "world", &traj_marker);
  //       mav_traj_marker_pub.publish(traj_marker);
  //     }
  //     mav_published = true;
  //   }
  // }
}

void trajTypeCallback(const std_msgs::String::ConstPtr& msg) {
  traj_type = msg->data;
}

void PublishMavReference(const ros::TimerEvent&) {
  if (!traj_type.empty()) {
    std::stringstream ss(traj_type);
    char t;
    ss >> t;
    if (t == 'p') {
      double x, y, z, v;
      ss >> x >> y >> z >> v;
      PublishSetPoint(x, y, z, v);
    } else if (t == 'c') {
      double r, v;
      ss >> r >> v;
      PublishCircularTrajectory(r, v);
    } else if (t == 'l') {
      double x, y, z, v;
      ss >> x >> y >> z >> v;
      PublishLine(x, y, z, v);
    } else if (t == 'g') {
      double v;
      ss >> v;
      PublishLine(v, 0);
    } else if (t == 'h') {
      double v;
      ss >> v;
      PublishLine(v, 1);
    } else if (t == 'm' && !mav_published) {
      double x, y, z;
      ss >> x >> y >> z;
      // define set point
      Eigen::Vector3d position, velocity;
      position << x, y, z;
      velocity << 0.0, 0.0, 0.0;

      double distance_to_goal = (odometry.position_W - position).norm();
      std::cout << "Distance to goal: " << distance_to_goal << std::endl;
      if (distance_to_goal < 0.2) {
        std::cout << "Published point reference" << std::endl;
        mav_msgs::EigenTrajectoryPoint ref;
        ref.position_W = position;
        ref.velocity_W << 0, 0, 0;
        trajectory_msgs::MultiDOFJointTrajectoryPoint ref_msg;
        mav_msgs::msgMultiDofJointTrajectoryPointFromEigen(ref, &ref_msg);
        mav_traj_pt_pub.publish(ref_msg);
        return;
      }

      mav_trajectory_generation::Trajectory trajectory;
      if (planTrajectory(position, velocity, &trajectory)) {
        mav_msgs::EigenTrajectoryPoint::Vector states;
        visualization_msgs::MarkerArray traj_marker;

        // Sample range:
        double t_start = 0.0;
        double dt = 0.1;
        double duration = std::min(mpc_plan_time, trajectory.getMaxTime());

        mav_trajectory_generation::sampleTrajectoryInRange(trajectory, t_start, duration, dt, &states);

        std::cout << "Total waypoints: " << states.size() << std::endl;

        trajectory_msgs::MultiDOFJointTrajectory mav_traj_msg;
        mav_msgs::msgMultiDofJointTrajectoryFromEigen(states, &mav_traj_msg);
        mav_traj_pub.publish(mav_traj_msg);

        // mav_trajectory_generation::drawMavTrajectory(trajectory, 0, "world", &traj_marker);
        mav_trajectory_generation::drawMavSampledTrajectorybyTime(states, dt, "world", &traj_marker);
        mav_traj_marker_pub.publish(traj_marker);
      }
    } else if (t == 'n') {
      double r, v;
      ss >> r >> v;
      mav_trajectory_generation::Trajectory trajectory;
      if (planCircularTrajectory(r, v, &trajectory)) {
        mav_msgs::EigenTrajectoryPoint::Vector states;
        visualization_msgs::MarkerArray traj_marker;

        // Sample range:
        double t_start = 0.0;
        double dt = 0.1;
        double duration = std::min(mpc_plan_time, trajectory.getMaxTime());

        mav_trajectory_generation::sampleTrajectoryInRange(trajectory, t_start, duration, dt, &states);

        std::cout << "Total waypoints: " << states.size() << std::endl;

        trajectory_msgs::MultiDOFJointTrajectory mav_traj_msg;
        mav_msgs::msgMultiDofJointTrajectoryFromEigen(states, &mav_traj_msg);
        mav_traj_pub.publish(mav_traj_msg);

        // mav_trajectory_generation::drawMavTrajectory(trajectory, 0, "world", &traj_marker);
        mav_trajectory_generation::drawMavSampledTrajectorybyTime(states, dt, "world", &traj_marker);
        mav_traj_marker_pub.publish(traj_marker);
      }
    }
  }
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "trajectory_publisher_node");
  ros::NodeHandle n;

  ros::Subscriber traj_type = n.subscribe("traj_type", 1, trajTypeCallback);
  ros::Subscriber sub_odom = n.subscribe("odometry", 1, OdometryCallback);
  mav_traj_pt_pub = n.advertise<trajectory_msgs::MultiDOFJointTrajectoryPoint>(
      "command/mav_trajectory_point", 1);
  mav_traj_pub = n.advertise<trajectory_msgs::MultiDOFJointTrajectory>(
      "command/mav_trajectory", 1);
  traj_pub = n.advertise<core_trajectory_msgs::TrajectoryXYZVYaw>(
      "command/current_trajectory", 1);
  setpoint_pub = n.advertise<core_trajectory_msgs::WaypointXYZVYaw>(
      "command/current_setpoint", 1);

  mav_traj_marker_pub = n.advertise<visualization_msgs::MarkerArray>("mav_reference_traj", 1);
  // traj_pub_vis = n.advertise<nav_msgs::Path>("mpc/reference_traj", 1);

  ros::Timer traj_loop =
      n.createTimer(ros::Duration(0.02), PublishMavReference);

  ros::spin();

  return 0;
}
