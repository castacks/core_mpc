#include <mav_msgs/common.h>
#include <mav_msgs/conversions.h>
#include <mav_msgs/eigen_mav_msgs.h>
#include <mavros_msgs/AttitudeTarget.h>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/Joy.h>
#include <tf/transform_datatypes.h>
#include <Eigen/Eigen>

ros::Publisher cmd_pub;
ros::Publisher rpyrt_cmd_pub;

double roll_limit_ = 45.0 * M_PI / 180.0;
double pitch_limit_ = 45.0 * M_PI / 180.0;
double yaw_rate_limit_ = 1.0;
double thrust_max_ = 25 * 1.555;

mav_msgs::EigenOdometry odometry;

// mavros_msgs::AttitudeTarget control_msg;
mav_msgs::RollPitchYawrateThrust rpyrt_msg;

void JoyCallback(const sensor_msgs::JoyConstPtr& msg) {
  // control_msg.type_mask = mavros_msgs::AttitudeTarget::IGNORE_ROLL_RATE |
  //                         mavros_msgs::AttitudeTarget::IGNORE_PITCH_RATE;
  // double roll = msg->axes[2] * roll_limit_;
  // double pitch = msg->axes[3] * pitch_limit_;
  // double yawrate = msg->axes[0] * yaw_rate_limit_;

  // tf::Quaternion q;
  // q.setRPY(roll, pitch, odometry.getYaw());

  // control_msg.orientation.w = q.w();
  // control_msg.orientation.x = q.x();
  // control_msg.orientation.y = q.y();
  // control_msg.orientation.z = q.z();

  rpyrt_msg.roll = msg->axes[2] * roll_limit_;
  rpyrt_msg.pitch = msg->axes[3] * pitch_limit_;
  rpyrt_msg.yaw_rate = msg->axes[0] * yaw_rate_limit_;
  rpyrt_msg.thrust.z = msg->axes[1];
  rpyrt_msg.header.stamp = ros::Time::now();
  // rpyrt_cmd_pub.publish(rpyrt_msg);

  // control_msg.body_rate.z = yawrate;
  // control_msg.thrust = msg->axes[1];
}

void OdometryCallback(const nav_msgs::Odometry::ConstPtr& msg) {
  mav_msgs::eigenOdometryFromMsg(*msg, &odometry);
  // control_msg.header.stamp = ros::Time::now();
  // cmd_pub.publish(control_msg);
  // rpyrt_msg.header.stamp = ros::Time::now();
  rpyrt_cmd_pub.publish(rpyrt_msg);
}

void ImuCallback(const sensor_msgs::ImuConstPtr& msg) {
  // control_msg.header.stamp = ros::Time::now();
  // cmd_pub.publish(control_msg);
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "fly_joy_node");
  ros::NodeHandle n;

  ros::Subscriber sub_joy = n.subscribe("/joy", 1, JoyCallback);
  ros::Subscriber sub_odom =
      n.subscribe("/uav1/odometry", 1, OdometryCallback);
  ros::Subscriber sub_imu = n.subscribe("/uav1/mavros/imu/data", 1, ImuCallback);
  // ros::Subscriber sub_imu = n.subscribe("/firefly/imu", 1, ImuCallback);
  // cmd_pub = n.advertise<mavros_msgs::AttitudeTarget>(
  //     "/mavros/setpoint_raw/attitude", 10);
  rpyrt_cmd_pub = n.advertise<mav_msgs::RollPitchYawrateThrust>(
      "/uav1/roll_pitch_yawrate_thrust_command", 10);
  // rpyrt_cmd_pub = n.advertise<mav_msgs::RollPitchYawrateThrust>(
  //     "/firefly/command/roll_pitch_yawrate_thrust", 10);
  ros::spin();

  return 0;
}
