#include <core_trajectory_msgs/TrajectoryXYZVYaw.h>
#include <core_trajectory_msgs/WaypointXYZVYaw.h>
#include <eigen_conversions/eigen_msg.h>
#include <mav_msgs/common.h>
#include <mav_msgs/conversions.h>
#include <mav_msgs/eigen_mav_msgs.h>
#include <mav_trajectory_generation/polynomial_optimization_nonlinear.h>
#include <mav_trajectory_generation/trajectory_sampling.h>
#include <mav_trajectory_generation_ros/ros_conversions.h>
#include <mav_trajectory_generation_ros/ros_visualization.h>
#include <trajectory_msgs/MultiDOFJointTrajectory.h>
#include <visualization_msgs/MarkerArray.h>
#include <nav_msgs/Path.h>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/String.h>

double mpc_plan_time = 2.0;
static constexpr int ACADO_N = 20;
mav_msgs::EigenOdometry odometry;
ros::Publisher traj_pub;
ros::Publisher mav_traj_pub;
ros::Publisher traj_pub_vis;
ros::Publisher setpoint_pub;
ros::Publisher mav_traj_pt_pub;
ros::Publisher mav_traj_marker_pub;
std::string traj_type;

Eigen::Affine3d current_pose_;
Eigen::Vector3d current_velocity_;
Eigen::Vector3d current_angular_velocity_;
double max_v_ = 5.0;
double max_a_ = 2.0;
double max_ang_v_ = 3.0;
double max_ang_a_ = 2.0;

// Plans a trajectory from the current position to the a goal position and
// velocity we neglect attitude here for simplicity
bool planTrajectory(const Eigen::VectorXd& goal_pos,
                    const Eigen::VectorXd& goal_vel,
                    mav_trajectory_generation::Trajectory* trajectory) {
  // 3 Dimensional trajectory => through carteisan space, no orientation
  const int dimension = 3;

  // Array for all waypoints and their constrains
  mav_trajectory_generation::Vertex::Vector vertices;

  // Optimze up to 4th order derivative (SNAP)
  const int derivative_to_optimize =
      mav_trajectory_generation::derivative_order::SNAP;

  // we have 2 vertices:
  // Start = current position
  // end = desired position and velocity
  mav_trajectory_generation::Vertex start(dimension), end(dimension);

  /******* Configure start point *******/
  // set start point constraints to current position and set all derivatives to
  // zero
  start.makeStartOrEnd(current_pose_.translation(), derivative_to_optimize);

  // set start point's velocity to be constrained to current velocity
  // start.addConstraint(mav_trajectory_generation::derivative_order::VELOCITY,
  //                     current_velocity_);

  // add waypoint to list
  vertices.push_back(start);

  mav_trajectory_generation::Vertex middle(dimension);
  middle.addConstraint(mav_trajectory_generation::derivative_order::POSITION, (current_pose_.translation() + goal_pos)/2.0);
  vertices.push_back(middle);

  /******* Configure end point *******/
  // set end point constraints to desired position and set all derivatives to
  // zero
  end.makeStartOrEnd(goal_pos, derivative_to_optimize);

  // set start point's velocity to be constrained to current velocity
  end.addConstraint(mav_trajectory_generation::derivative_order::VELOCITY,
                    goal_vel);

  // add waypoint to list
  vertices.push_back(end);

  // estimate initial segment times
  std::vector<double> segment_times;
  segment_times = estimateSegmentTimes(vertices, max_v_, max_a_);

  // Set up polynomial solver with default params
  mav_trajectory_generation::NonlinearOptimizationParameters parameters;

  // set up optimization problem
  const int N = 10;
  mav_trajectory_generation::PolynomialOptimizationNonLinear<N> opt(dimension,
                                                                    parameters);
  opt.setupFromVertices(vertices, segment_times, derivative_to_optimize);

  // constrain velocity and acceleration
  opt.addMaximumMagnitudeConstraint(
      mav_trajectory_generation::derivative_order::VELOCITY, max_v_);
  opt.addMaximumMagnitudeConstraint(
    mav_trajectory_generation::derivative_order::ANGULAR_VELOCITY, max_ang_v_);
  opt.addMaximumMagnitudeConstraint(
      mav_trajectory_generation::derivative_order::ACCELERATION, max_a_);
      
  // solve trajectory
  opt.optimize();

  // get trajectory as polynomial parameters
  opt.getTrajectory(&(*trajectory));

  return true;
}

void OdometryCallback(const nav_msgs::Odometry::ConstPtr& msg) {
  mav_msgs::eigenOdometryFromMsg(*msg, &odometry);

  // store current position in our planner
  tf::poseMsgToEigen(msg->pose.pose, current_pose_);

  // store current velocity
  tf::vectorMsgToEigen(msg->twist.twist.linear, current_velocity_);
  tf::vectorMsgToEigen(msg->twist.twist.angular, current_angular_velocity_);
}

void trajTypeCallback(const std_msgs::String::ConstPtr& msg) {
  traj_type = msg->data;
}

void PublishMavReference(const ros::TimerEvent&) {
  if (!traj_type.empty()) {
    std::stringstream ss(traj_type);
    char t;
    ss >> t;
    if (t == 'm') {
      double x, y, z;
      ss >> x >> y >> z;
      // define set point
      Eigen::Vector3d position, velocity;
      position << x, y, z;
      velocity << 0.0, 0.0, 0.0;

      double distance_to_goal = (odometry.position_W - position).norm();
      std::cout << "Distance to goal: " << distance_to_goal << std::endl;
      if (distance_to_goal < 0.2) {
        std::cout << "Published point reference" << std::endl;
        mav_msgs::EigenTrajectoryPoint ref;
        ref.position_W = position;
        ref.velocity_W << 0, 0, 0;
        trajectory_msgs::MultiDOFJointTrajectoryPoint ref_msg;
        mav_msgs::msgMultiDofJointTrajectoryPointFromEigen(ref, &ref_msg);
        mav_traj_pt_pub.publish(ref_msg);
        return;
      }

      mav_trajectory_generation::Trajectory trajectory;
      if (planTrajectory(position, velocity, &trajectory)) {
        mav_msgs::EigenTrajectoryPoint::Vector states;
        visualization_msgs::MarkerArray traj_marker;

        // Sample range:
        double t_start = 0.0;
        double dt = 0.008;
        double duration = std::min(mpc_plan_time, trajectory.getMaxTime());

        mav_trajectory_generation::sampleTrajectoryInRange(trajectory, t_start, duration, dt, &states);

        std::cout << "Total waypoints: " << states.size() << std::endl;

        trajectory_msgs::MultiDOFJointTrajectory mav_traj_msg;
        mav_msgs::msgMultiDofJointTrajectoryFromEigen(states, &mav_traj_msg);
        mav_traj_pub.publish(mav_traj_msg);

        // mav_trajectory_generation::drawMavTrajectory(trajectory, 0, "world", &traj_marker);
        mav_trajectory_generation::drawMavSampledTrajectorybyTime(states, dt, "world", &traj_marker);
        mav_traj_marker_pub.publish(traj_marker);
      }
    } else if (t == 'n') {
      double x, y, z;
      ss >> x >> y >> z;
      Eigen::Vector3d position;
      position << x, y, z;
      mav_msgs::EigenTrajectoryPoint ref;
      ref.position_W = position;
      ref.velocity_W << 0, 0, 0;
      trajectory_msgs::MultiDOFJointTrajectoryPoint ref_msg;
      mav_msgs::msgMultiDofJointTrajectoryPointFromEigen(ref, &ref_msg);
      mav_traj_pt_pub.publish(ref_msg);
    }
  }
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "trajectory_publisher_node");
  ros::NodeHandle n;

  ros::Subscriber traj_type = n.subscribe("traj_type", 1, trajTypeCallback);
  ros::Subscriber sub_odom = n.subscribe("odometry", 1, OdometryCallback);
  mav_traj_pt_pub = n.advertise<trajectory_msgs::MultiDOFJointTrajectoryPoint>(
      "command/mav_trajectory_point", 1);
  mav_traj_pub = n.advertise<trajectory_msgs::MultiDOFJointTrajectory>(
      "command/mav_trajectory", 1);

  mav_traj_marker_pub = n.advertise<visualization_msgs::MarkerArray>("mav_reference_traj", 1);

  ros::Timer traj_loop =
      n.createTimer(ros::Duration(0.02), PublishMavReference);

  ros::spin();

  return 0;
}
