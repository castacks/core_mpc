#include <mpc/mpc_rate.h>

// Public functions

MPCRate::MPCRate(const ros::NodeHandle& nh)
    : nh_(nh),
      received_first_odometry_(false),
      position_error_integration_(0, 0, 0),
      mpc_queue_(nh, ACADO_N + 1) {
  trajectory_reference_vis_publisher_ =
      nh_.advertise<nav_msgs::Path>("mpc/reference_traj", 1);
  publish_reference_vis_timer_ = nh_.createTimer(
      ros::Duration(0.1), &MPCRate::PublishReferenceVisualization, this);

  acado_initializeSolver();

  W_.setZero();
  WN_.setZero();

  input_.setZero();
  state_.setZero();
  reference_.setZero();
  referenceN_.setZero();

  predicted_path_pub_ =
      nh_.advertise<nav_msgs::Path>("mpc/trajectory_predicted", 1);
}

MPCRate::~MPCRate() {}

void MPCRate::SetMPCParameters(mpc::RateParamsConfig& config) {
  sampling_time_ = config.sampling_time;
  prediction_sampling_time_ = config.prediction_sampling_time;
  mpc_queue_.initializeQueue(sampling_time_, prediction_sampling_time_);

  enable_integrator_ = config.enable_integrator;
  Ki_altitude_ = config.Ki_altitude;
  Ki_xy_ = config.Ki_xy;
  antiwindup_ball_ = config.antiwindup_ball;
  position_error_integration_limit_ = config.position_error_integration_limit;

  q_position_ << config.q_x, config.q_y, config.q_z;
  q_attitude_ << config.q_qw, config.q_qx, config.q_qy, config.q_qz;
  q_velocity_ << config.q_vx, config.q_vy, config.q_vz;
  r_command_ << config.r_thrust, config.r_wx, config.r_wy, config.r_wz;

  W_.block(0, 0, 3, 3) = q_position_.asDiagonal();
  W_.block(3, 3, 3, 3) = q_velocity_.asDiagonal();
  W_.block(6, 6, 4, 4) = q_attitude_.asDiagonal();
  W_.block(10, 10, 4, 4) = r_command_.asDiagonal();

  WN_ = W_.block(0, 0, 6, 6);

  Eigen::Map<Eigen::Matrix<double, ACADO_NY, ACADO_NY>>(
      const_cast<double*>(acadoVariables.W)) = W_.transpose();
  Eigen::Map<Eigen::Matrix<double, ACADO_NYN, ACADO_NYN>>(
      const_cast<double*>(acadoVariables.WN)) = WN_.transpose();

  rp_rate_limit_ = config.rp_rate_max;
  yaw_rate_limit_ = config.yaw_rate_max;
  thrust_min_ = config.thrust_min;
  thrust_max_ = config.thrust_max;
  roll_max = config.roll_max;
  pitch_max = config.pitch_max;

  mass_ = config.mass;

  for (size_t i = 0; i < ACADO_N; ++i) {
    acadoVariables.lbValues[4 * i] = thrust_min_;
    acadoVariables.lbValues[4 * i + 1] = -rp_rate_limit_;
    acadoVariables.lbValues[4 * i + 2] = -rp_rate_limit_;
    acadoVariables.lbValues[4 * i + 3] = -yaw_rate_limit_;
    acadoVariables.ubValues[4 * i] = thrust_max_;
    acadoVariables.ubValues[4 * i + 1] = rp_rate_limit_;
    acadoVariables.ubValues[4 * i + 2] = rp_rate_limit_;
    acadoVariables.ubValues[4 * i + 3] = yaw_rate_limit_;

    // acadoVariables.lbAValues[i] = -0.45;
    // acadoVariables.lbAValues[2 * i + 1] = -0.45;
    // acadoVariables.ubAValues[i] = 0.45;
    // acadoVariables.ubAValues[2 * i + 1] = 0.45;
  }

  std::cout << "MPC Params set!" << std::endl;
  std::cout << "W = \n" << W_ << std::endl;
  std::cout << "WN_ = \n" << WN_ << std::endl;
}

void MPCRate::SetOdometry(const mav_msgs::EigenOdometry& odometry) {
  if (!received_first_odometry_) {
    Eigen::VectorXd x0(ACADO_NX);

    x0 << odometry.position_W, odometry.orientation_W_B.w(),
        odometry.orientation_W_B.x(), odometry.orientation_W_B.y(),
        odometry.orientation_W_B.z(), odometry.getVelocityWorld();

    InitializeAcadoSolver(x0);
    received_first_odometry_ = true;
  }
  odometry_.position_W = odometry.position_W;
  odometry_.velocity_B = odometry.velocity_B;
  odometry_.angular_velocity_B = odometry.angular_velocity_B;
  odometry_.orientation_W_B = odometry.orientation_W_B;
  odometry_.timestamp_ns = odometry.timestamp_ns;
}

void MPCRate::SetReferenceTrajectory(
    const trajectory_msgs::MultiDOFJointTrajectoryConstPtr& msg) {
  mav_msgs::EigenTrajectoryPointDeque trajectory;
  mav_msgs::eigenTrajectoryPointDequeFromMsg(*msg, &trajectory);
  mpc_queue_.insertReferenceTrajectory(trajectory);
}

void MPCRate::SetReferencePose(
    const trajectory_msgs::MultiDOFJointTrajectoryPointConstPtr& msg) {
  mav_msgs::EigenTrajectoryPoint ref;
  mav_msgs::eigenTrajectoryPointFromMsg(*msg, &ref);
  mpc_queue_.insertReference(ref);
}

void MPCRate::Solve(Eigen::Vector4d* control_ref) {
  Eigen::Matrix<double, ACADO_NX, 1> x_0;
  Eigen::Vector3d estimated_error;

  mpc_queue_.updateQueue();
  mpc_queue_.getQueue(position_ref_, orientation_ref_, velocity_ref_,
                      angular_velocity_ref_, acceleration_ref_, yaw_ref_,
                      yaw_rate_ref_);

  x_0 << odometry_.position_W, odometry_.orientation_W_B.w(),
      odometry_.orientation_W_B.x(), odometry_.orientation_W_B.y(),
      odometry_.orientation_W_B.z(), odometry_.getVelocityWorld();

  Eigen::Vector3d current_rpy;
  odometry_.getEulerAngles(&current_rpy);

  Eigen::Vector3d acceleration;
  const Eigen::Vector3d gravity(0.0, 0.0, -kGravity);
  Eigen::Quaterniond q_heading;
  Eigen::Quaterniond q_orientation;
  Eigen::Vector3d angular_velocity_ref_B;

  for (size_t i = 0; i < ACADO_N; i++) {
    q_heading = mav_msgs::quaternionFromYaw(yaw_ref_[i]);
    q_orientation = q_heading * orientation_ref_[i];
    acceleration = acceleration_ref_[i] - gravity;
    angular_velocity_ref_B =
        odometry_.orientation_W_B.toRotationMatrix().transpose() *
        angular_velocity_ref_[i];

    reference_.block(i, 0, 1, ACADO_NY) << position_ref_[i].transpose(),
        velocity_ref_[i].transpose(), q_orientation.w(), q_orientation.x(),
        q_orientation.y(), q_orientation.z(), acceleration.norm(),
        angular_velocity_ref_B.transpose();

    if (reference_.row(i).segment(6, 4).dot(x_0.segment(3, 4)) < 0.0)
      reference_.block(i, 6, 1, 4) = -reference_.block(i, 6, 1, 4);
  }

  referenceN_ << position_ref_[ACADO_N].transpose(),
      velocity_ref_[ACADO_N].transpose();

  Eigen::Map<Eigen::Matrix<double, ACADO_NX, 1>>(
      const_cast<double*>(acadoVariables.x0)) = x_0;
  Eigen::Map<Eigen::Matrix<double, ACADO_NX, ACADO_N + 1>>(
      const_cast<double*>(acadoVariables.x)) = state_.transpose();
  Eigen::Map<Eigen::Matrix<double, ACADO_NY, ACADO_N>>(
      const_cast<double*>(acadoVariables.y)) = reference_.transpose();
  Eigen::Map<Eigen::Matrix<double, ACADO_NYN, 1>>(
      const_cast<double*>(acadoVariables.yN)) = referenceN_.transpose();

  // std::cout << "W = \n" << W_ << std::endl;
  // std::cout << "WN_ = \n" << WN_ << std::endl;

  std::cout << "Reference:" << std::endl;
  std::cout << reference_ << std::endl;

  std::cout << "Reference N:" << std::endl;
  std::cout << referenceN_ << std::endl;

  std::cout << "x0:" << std::endl;
  std::cout << x_0 << std::endl;

  acado_preparationStep();

  int acado_status = acado_feedbackStep();
  printf("Real-Time Iteration:  KKT Tolerance = %.3e", acado_getKKT());

  double thrust_ref = acadoVariables.u[0] * mass_;
  double wx_ref = acadoVariables.u[1];
  double wy_ref = acadoVariables.u[2];
  double wz_ref = acadoVariables.u[3];

  // std::cout << "Solved control: " << wx_ref << ", " << wy_ref << ", " <<
  // wz_ref
  //           << ", " << thrust_ref << std::endl;

  state_ =
      Eigen::Map<Eigen::Matrix<double, ACADO_N + 1, ACADO_NX, Eigen::RowMajor>>(
          acadoVariables.x);
  input_ =
      Eigen::Map<Eigen::Matrix<double, ACADO_N, ACADO_NU, Eigen::RowMajor>>(
          acadoVariables.u);

  // std::cout << "State: " << std::endl;
  // std::cout << state_ << std::endl;

  if (std::isnan(wx_ref) || std::isnan(wy_ref) || std::isnan(wz_ref) ||
      std::isnan(thrust_ref) || acado_status != 0) {
    ROS_WARN_STREAM("MPC failed with status: " << acado_status);
    ROS_WARN("Reinitializing...");
    InitializeAcadoSolver(x_0);
    *control_ref << 0, 0, 0, kGravity * mass_;
    return;
  }

  // if (current_rpy.x() >= roll_max || current_rpy.x() <= -roll_max) {
  //   std::cout << "exceeded roll, commanded roll rate: " << wx_ref << std::endl;
  //   wx_ref = 0;
  // }
  // if (current_rpy.y() >= pitch_max || current_rpy.y() <= -pitch_max) {
  //   std::cout << "exceeded pitch, commanded pitch rate: " << wy_ref << std::endl;
  //   wy_ref = 0;
  // }

  *control_ref << wx_ref, wy_ref, wz_ref, thrust_ref;

  PublishPredictedPath();
}

void MPCRate::PublishPredictedPath() {
  nav_msgs::Path path_msg;
  path_msg.header.stamp = ros::Time::now();
  path_msg.header.frame_id = "world";
  geometry_msgs::PoseStamped pose;

  for (int i = 0; i < ACADO_N + 1; i++) {
    pose.header.stamp =
        ros::Time::now() + ros::Duration(i * prediction_sampling_time_);
    pose.header.seq = i;
    pose.pose.position.x = state_(i, 0);
    pose.pose.position.y = state_(i, 1);
    pose.pose.position.z = state_(i, 2);
    path_msg.poses.push_back(pose);
  }
  predicted_path_pub_.publish(path_msg);
}

// Private functions

void MPCRate::InitializeAcadoSolver(Eigen::VectorXd x0) {
  for (int i = 0; i < ACADO_N + 1; i++) {
    state_.block(i, 0, 1, ACADO_NX) << x0.transpose();
  }

  Eigen::Map<Eigen::Matrix<double, ACADO_NX, ACADO_N + 1>>(
      const_cast<double*>(acadoVariables.x)) = state_.transpose();
  Eigen::Map<Eigen::Matrix<double, ACADO_NU, ACADO_N>>(
      const_cast<double*>(acadoVariables.u)) = input_.transpose();
  Eigen::Map<Eigen::Matrix<double, ACADO_NY, ACADO_N>>(
      const_cast<double*>(acadoVariables.y)) = reference_.transpose();
  Eigen::Map<Eigen::Matrix<double, ACADO_NYN, 1>>(
      const_cast<double*>(acadoVariables.yN)) = referenceN_.transpose();
}

void MPCRate::PublishReferenceVisualization(const ros::TimerEvent&) {
  if (trajectory_reference_vis_publisher_.getNumSubscribers() > 0) {
    nav_msgs::Path vis_msg;
    vis_msg.header.stamp = ros::Time::now();
    vis_msg.header.frame_id = "world";
    geometry_msgs::PoseStamped vis_pose;

    for (int i = 0; i < position_ref_.size(); i++) {
      vis_pose.header.stamp =
          ros::Time::now() + ros::Duration(i * prediction_sampling_time_);
      vis_pose.header.seq = i;
      vis_pose.pose.position.x = position_ref_[i].x();
      vis_pose.pose.position.y = position_ref_[i].y();
      vis_pose.pose.position.z = position_ref_[i].z();
      vis_msg.poses.push_back(vis_pose);
    }

    trajectory_reference_vis_publisher_.publish(vis_msg);
  }
}