#include <core_trajectory_msgs/TrajectoryXYZVYaw.h>
#include <core_trajectory_msgs/WaypointXYZVYaw.h>
#include <dynamic_reconfigure/server.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <lapacke.h>
#include <mav_msgs/common.h>
#include <mav_msgs/conversions.h>
#include <mav_msgs/eigen_mav_msgs.h>
#include <mpc/ParamsConfig.h>
#include <nav_msgs/Path.h>
#include <ros/ros.h>
#include <stdio.h>
#include <string.h>
#include <trajectory_msgs/MultiDOFJointTrajectory.h>
#include <Eigen/Eigen>
#include "acado_auxiliary_functions.h"
#include "acado_common.h"
#include "mpc_queue.h"

ACADOvariables acadoVariables;
ACADOworkspace acadoWorkspace;
lapack_logical select_lhp(const double* real, const double* imag) {
  return *real < 0.0;
}
class MPC {
 public:
  MPC(const ros::NodeHandle& nh);
  ~MPC();

  void SetMPCParameters(mpc::ParamsConfig& config);
  void SetOdometry(const mav_msgs::EigenOdometry& odometry);
  void SetReferencePose(
      const trajectory_msgs::MultiDOFJointTrajectoryPointConstPtr& msg);
  void SetReferencePose(core_trajectory_msgs::WaypointXYZVYaw& msg);
  void SetReferencePose(
      const core_trajectory_msgs::WaypointXYZVYawConstPtr& msg);
  void SetReferenceTrajectory(
      const core_trajectory_msgs::TrajectoryXYZVYawConstPtr& msg);
  void SetReferenceTrajectory(
      const trajectory_msgs::MultiDOFJointTrajectoryConstPtr& msg);
  void PublishPredictedPath();
  void Solve(Eigen::Vector4d* control_ref);

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
 private:
  // constants
  static constexpr double kGravity = 9.8066;

  // ros nodes and publishers
  ros::NodeHandle nh_;
  ros::Publisher predicted_path_pub_;

  // sampling time parameters
  double sampling_time_;
  double prediction_sampling_time_;

  // model parameters
  double mass_;
  double roll_tau_;
  double roll_gain_;
  double pitch_tau_;
  double pitch_gain_;
  double drag_coeff_x_;
  double drag_coeff_y_;

  // state penalty
  Eigen::Vector3d q_position_;
  Eigen::Vector3d q_velocity_;
  Eigen::Vector3d q_attitude_;

  // control penalty
  Eigen::Vector4d r_command_;

  // control input limits
  double roll_limit_ = 45.0 * M_PI / 180.0;
  double pitch_limit_ = 45.0 * M_PI / 180.0;
  double yaw_rate_limit_ = 1.5;
  double thrust_min_ = kGravity / 2.0;
  double thrust_max_ = kGravity * 2.0;

  // solver matrices
  Eigen::Matrix<double, ACADO_NY, ACADO_NY> W_;
  Eigen::Matrix<double, ACADO_NYN, ACADO_NYN> WN_;
  Eigen::Matrix<double, ACADO_N + 1, ACADO_NX> state_;
  Eigen::Matrix<double, ACADO_N, ACADO_NU> input_;
  Eigen::Matrix<double, ACADO_N, ACADO_NY> reference_;
  Eigen::Matrix<double, 1, ACADO_NYN> referenceN_;
  Eigen::Matrix<double, ACADO_N + 1, ACADO_NOD> acado_online_data_;

  // commands
  Eigen::Vector4d command_roll_pitch_yaw_thrust_;

  // most recent odometry information
  mav_msgs::EigenOdometry odometry_;
  bool received_first_odometry_;

  // current references
  MPCQueue mpc_queue_;
  Vector3dDeque position_ref_, velocity_ref_, acceleration_ref_;
  std::deque<double> yaw_ref_, yaw_rate_ref_;

  ros::Publisher trajectory_reference_vis_publisher_;
  void PublishReferenceVisualization(const ros::TimerEvent&);
  ros::Timer publish_reference_vis_timer_;

  // error integrator
  bool enable_integrator_;
  double Ki_altitude_;
  double Ki_xy_;
  double antiwindup_ball_;
  Eigen::Vector3d position_error_integration_;
  double position_error_integration_limit_;

  // initilize solver
  void InitializeAcadoSolver(Eigen::VectorXd x0);
  Eigen::MatrixXd solveCARE(Eigen::MatrixXd Q, Eigen::MatrixXd R);
};
