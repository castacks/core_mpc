#include <memory>
#include <acado_optimal_control.hpp>
#include <acado_code_generation.hpp>
#include <acado_gnuplot.hpp>

// Standalone code generation for a parameter-free quadrotor model
// with thrust and rates input. 

int main( ){
  // Use Acado
  USING_NAMESPACE_ACADO

  /*
  Switch between code generation and analysis.
  If CODE_GEN is true the system is compiled into an optimizaiton problem
  for real-time iteration and all code to run it online is generated.
  Constraints and reference structure is used but the values will be set on
  runtinme.
  If CODE_GEN is false, the system is compiled into a standalone optimization
  and solved on execution. The reference and constraints must be set in here.
  */
  const bool CODE_GEN = true;

  // System variables
  DifferentialState     p_x, p_y, p_z;
  DifferentialState     q_w, q_x, q_y, q_z;
  DifferentialState     v_x, v_y, v_z;
  Control               T, w_x, w_y, w_z;
  DifferentialEquation  f;
  Function              h, hN;

  Expression roll = atan((2.0 * (q_w * q_x + q_y * q_z)) / (1.0 - 2.0 * (q_x * q_x + q_y * q_y)));
  Expression pitch = asin(2.0 * (q_w * q_y - q_z * q_x));

  // Parameters with exemplary values. These are set/overwritten at runtime.
  const double t_start = 0.0;     // Initial time [s]
  const double t_end = 2.0;       // Time horizon [s]
  const double dt = 0.1;          // Discretization time [s]
  const int N = round(t_end/dt);  // Number of nodes
  const double g_z = 9.8066;      // Gravity is everywhere [m/s^2]
  const double w_max_yaw = 1;     // Maximal yaw rate [rad/s]
  const double w_max_xy = 3;      // Maximal pitch and roll rate [rad/s]
  const double T_min = 2;         // Minimal thrust [N]
  const double T_max = 20;        // Maximal thrust [N]

  // Bias to prevent division by zero.
  const double epsilon = 0.1;     // Camera projection recover bias [m]


  // System Dynamics
  f << dot(p_x) ==  v_x;
  f << dot(p_y) ==  v_y;
  f << dot(p_z) ==  v_z;
  f << dot(q_w) ==  0.5 * ( - w_x * q_x - w_y * q_y - w_z * q_z);
  f << dot(q_x) ==  0.5 * ( w_x * q_w + w_z * q_y - w_y * q_z);
  f << dot(q_y) ==  0.5 * ( w_y * q_w - w_z * q_x + w_x * q_z);
  f << dot(q_z) ==  0.5 * ( w_z * q_w + w_y * q_x - w_x * q_y);
  f << dot(v_x) ==  2 * ( q_w * q_y + q_x * q_z ) * T;
  f << dot(v_y) ==  2 * ( q_y * q_z - q_w * q_x ) * T;
  f << dot(v_z) ==  ( 1 - 2 * q_x * q_x - 2 * q_y * q_y ) * T - g_z;

  // Cost: Sum(i=0, ..., N-1){h_i' * Q * h_i} + h_N' * Q_N * h_N
  // Running cost vector consists of all states and inputs.
  h << p_x << p_y << p_z
    << v_x << v_y << v_z
    << q_w << q_x << q_y << q_z
    << T << w_x << w_y << w_z;

  // End cost vector consists of all states (no inputs at last state).
  hN << p_x << p_y << p_z
     << v_x << v_y << v_z;

  // DEFINE AN OPTIMAL CONTROL PROBLEM:
  // ----------------------------------
  OCP ocp( t_start, t_end, N );

  // For code generation, references are set during run time.
  BMatrix W  = eye<bool>(h.getDim());
  BMatrix WN = eye<bool>(hN.getDim());
  ocp.minimizeLSQ(W, h);
  ocp.minimizeLSQEndTerm(WN, hN);

  // Add system dynamics
  ocp.subjectTo( f );
  // Add constraints
  // ocp.subjectTo(-0.35 <= roll <= 0.35);
  // ocp.subjectTo(-0.45 <= pitch <= 0.45);
  ocp.subjectTo(-w_max_xy <= w_x <= w_max_xy);
  ocp.subjectTo(-w_max_xy <= w_y <= w_max_xy);
  ocp.subjectTo(-w_max_yaw <= w_z <= w_max_yaw);
  ocp.subjectTo(T_min <= T <= T_max);

  // For code generation, we can set some properties.
  // The main reason for a setting is given as comment.
  OCPexport mpc(ocp);

  mpc.set(HESSIAN_APPROXIMATION,  GAUSS_NEWTON);        // is robust, stable
  mpc.set(DISCRETIZATION_TYPE,    MULTIPLE_SHOOTING);   // good convergence
  mpc.set(SPARSE_QP_SOLUTION,     FULL_CONDENSING_N2);  // due to qpOASES
  mpc.set(INTEGRATOR_TYPE,        INT_IRK_GL4);         // accurate
  mpc.set(NUM_INTEGRATOR_STEPS,   N);
  mpc.set(QP_SOLVER,              QP_QPOASES);          // free, source code
  mpc.set(HOTSTART_QP,            YES);
  mpc.set(CG_USE_OPENMP,                    YES);       // paralellization
  mpc.set(CG_HARDCODE_CONSTRAINT_VALUES,    NO);        // set on runtime
  mpc.set(CG_USE_VARIABLE_WEIGHTING_MATRIX, NO);       // time-varying costs

  // Do not generate tests, makes or matlab-related interfaces.
  mpc.set( GENERATE_TEST_FILE,          NO);
  mpc.set( GENERATE_MAKE_FILE,          NO);
  mpc.set( GENERATE_MATLAB_INTERFACE,   NO);
  mpc.set( GENERATE_SIMULINK_INTERFACE, NO);

  // mpc.set( HESSIAN_APPROXIMATION, GAUSS_NEWTON);
  // mpc.set( DISCRETIZATION_TYPE, MULTIPLE_SHOOTING);
  // mpc.set( SPARSE_QP_SOLUTION, FULL_CONDENSING_N2); //FULL_CONDENsinG_N2
  // mpc.set( INTEGRATOR_TYPE, INT_IRK_GL4);
  // // mpc.set( NUM_INTEGRATOR_STEPS, N);
  // mpc.set( QP_SOLVER, QP_QPOASES);
  // mpc.set( HOTSTART_QP, NO);
  // mpc.set( LEVENBERG_MARQUARDT, 1e-10);
  // mpc.set( LINEAR_ALGEBRA_SOLVER, GAUSS_LU);
  // mpc.set( IMPLICIT_INTEGRATOR_NUM_ITS, 2);
  // mpc.set( CG_USE_OPENMP, YES);
  // mpc.set( CG_HARDCODE_CONSTRAINT_VALUES, NO);
  // mpc.set( CG_USE_VARIABLE_WEIGHTING_MATRIX, NO);

  // Finally, export everything.
  if(mpc.exportCode("quadrotor_rate_mpc_codegen") != SUCCESSFUL_RETURN)
    exit( EXIT_FAILURE );
  mpc.printDimensionsQP( );

  return EXIT_SUCCESS;
}