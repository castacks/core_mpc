#include <memory>
#include <acado_optimal_control.hpp>
#include <acado_code_generation.hpp>
#include <acado_gnuplot.hpp>


int main( ){
  USING_NAMESPACE_ACADO

  const bool CODE_GEN = true;

  // System variables
  DifferentialState     p_x, p_y, p_z;
  DifferentialState     roll, pitch, yaw;
  DifferentialState     v_x, v_y, v_z;
  Control               roll_cmd, pitch_cmd, yawrate_cmd, T;
  DifferentialEquation  f;
  Function              h, hN;

  OnlineData roll_tau;
  OnlineData roll_gain;
  OnlineData pitch_tau;
  OnlineData pitch_gain;
  OnlineData linear_drag_coefficient1;
  OnlineData linear_drag_coefficient2;

  // Non-linear drag
  IntermediateState dragacc1 =   sin(pitch)*linear_drag_coefficient1*T*v_z
                                + cos(pitch)*cos(yaw)*linear_drag_coefficient1*T*v_x
                                - cos(pitch)*linear_drag_coefficient1*sin(yaw)*T*v_y;
  IntermediateState dragacc2 =   (cos(roll)*sin(yaw) - cos(yaw)*sin(pitch)*sin(roll))*linear_drag_coefficient2*T*v_x
                                - (cos(roll)*cos(yaw) + sin(pitch)*sin(roll)*sin(yaw))*linear_drag_coefficient2*T*v_y
                                - cos(pitch)*linear_drag_coefficient2*sin(roll)*T*v_z;


  // Parameters with exemplary values. These are set/overwritten at runtime.
  const double t_start = 0.0;     // Initial time [s]
  const double t_end = 2.0;       // Time horizon [s]
  const double dt = 0.1;          // Discretization time [s]
  const int N = round(t_end/dt);  // Number of nodes
  const double g = 9.8066;
  const double PI = 3.1415926535897932;
  const double m = 1.56; // vehicle mass

  // System Dynamics
  f << dot(p_x) == v_x;
  f << dot(p_y) == v_y;
  f << dot(p_z) == v_z;
  f << dot(roll) == (roll_gain*roll_cmd - roll)/roll_tau;
  f << dot(pitch) == (pitch_gain*pitch_cmd - pitch)/pitch_tau;
  f << dot(yaw) == yawrate_cmd;
  f << dot(v_x) == ((cos(roll)*cos(yaw)*sin(pitch) + sin(roll)*sin(yaw))*T - dragacc1);
  f << dot(v_y) == ((cos(roll)*sin(pitch)*sin(yaw) - cos(yaw)*sin(roll))*T - dragacc2);
  f << dot(v_z) == (-g + cos(pitch)*cos(roll)*T);
  
  // Cost: Sum(i=0, ..., N-1){h_i' * Q * h_i} + h_N' * Q_N * h_N
  // Running cost vector consists of all states and inputs.
  h << p_x << p_y << p_z
    << v_x << v_y << v_z
    << roll << pitch << yaw
    << roll_cmd << pitch_cmd << yawrate_cmd << (-g + cos(pitch)*cos(roll)*T);

  // End cost vector consists of all states (no inputs at last state).
  hN << p_x << p_y << p_z
     << v_x << v_y << v_z;

  // Running cost weight matrix
  DMatrix Q(h.getDim(), h.getDim());
  Q.setIdentity();
  Q(0,0) = 50;   // x
  Q(1,1) = 50;   // y
  Q(2,2) = 80;   // z
  Q(3,3) = 20;   // roll
  Q(4,4) = 20;   // pitch
  Q(5,5) = 20;   // yaw
  Q(6,6) = 20;    // vx
  Q(7,7) = 20;    // vy
  Q(8,8) = 35;    // vz
  Q(9,9) = 30;     // roll_cmd
  Q(10,10) = 30;   // pitch_cmd
  Q(11,11) = 30;   // yawrate_cmd
  Q(12,12) = 5;   // T

  // End cost weight matrix
  DMatrix QN(hN.getDim(), hN.getDim());
  QN.setIdentity();
  QN(0,0) = Q(0,0);   // x
  QN(1,1) = Q(1,1);   // y
  QN(2,2) = Q(2,2);   // z
  QN(3,3) = Q(3,3);   // vx
  QN(4,4) = Q(4,4);   // vy
  QN(5,5) = Q(5,5);   // vz

  // Set a reference for the analysis (if CODE_GEN is false).
  DVector r(h.getDim());    // Running cost reference
  r.setZero();
  r(2) = 3.0;
  // r(12) = g;

  DVector rN(hN.getDim());   // End cost reference
  rN.setZero();
  rN(2) = r(2);


  // DEFINE AN OPTIMAL CONTROL PROBLEM:
  // ----------------------------------
  OCP ocp( t_start, t_end, N );
  // Add system dynamics
  ocp.subjectTo( f );
  
  if(!CODE_GEN)
  {
    // For analysis, set references.
    ocp.minimizeLSQ( Q, h, r );
    ocp.minimizeLSQEndTerm( QN, hN, rN );
  } else {
    // For code generation, references are set during run time.
//    BMatrix Q_sparse(h.getDim(), h.getDim());
//    Q_sparse.setIdentity();
//    BMatrix QN_sparse(hN.getDim(), hN.getDim());
//    QN_sparse.setIdentity();
//    ocp.minimizeLSQ( Q_sparse, h);
//    ocp.minimizeLSQEndTerm( QN_sparse, hN );
    BMatrix W  = eye<bool>(h.getDim());
    BMatrix WN = eye<bool>(hN.getDim());
    ocp.minimizeLSQ(W, h);
    ocp.minimizeLSQEndTerm(WN, hN);
  }
  // Add constraints
  ocp.subjectTo(-45*PI/180 <= roll_cmd    <= 45*PI/180);
  ocp.subjectTo(-45*PI/180 <= pitch_cmd   <= 45*PI/180);
  ocp.subjectTo(        -2 <= yawrate_cmd <= 2);
  ocp.subjectTo(     g/2.0 <= T           <= g*4.0);

  ocp.setNOD(6);


  if(!CODE_GEN)
  {
    // Set initial state
    ocp.subjectTo( AT_START, p_x ==  0.0 );
    ocp.subjectTo( AT_START, p_y ==  0.0 );
    ocp.subjectTo( AT_START, p_z ==  0.0 );
    ocp.subjectTo( AT_START, roll == 0.0 );
    ocp.subjectTo( AT_START, pitch ==  0.0 );
    ocp.subjectTo( AT_START, yaw ==  0.0 );
    ocp.subjectTo( AT_START, v_x ==  0.0 );
    ocp.subjectTo( AT_START, v_y ==  0.0 );
    ocp.subjectTo( AT_START, v_z ==  0.0 );

    //ocp.subjectTo( AT_START, roll_tau ==  0.257 );
    //ocp.subjectTo( AT_START, roll_gain ==  0.75 );
    //ocp.subjectTo( AT_START, pitch_tau ==  0.259 );
    //ocp.subjectTo( AT_START, pitch_gain ==  0.78 );

    // Setup some visualization
    GnuplotWindow window1( PLOT_AT_EACH_ITERATION );
    window1.addSubplot( p_x,"position x" );
    window1.addSubplot( p_y,"position y" );
    window1.addSubplot( p_z,"position z" );
    window1.addSubplot( roll,"roll" );
    window1.addSubplot( pitch,"pitch" );
    window1.addSubplot( yaw,"yaw" );
    window1.addSubplot( v_x,"verlocity x" );
    window1.addSubplot( v_y,"verlocity y" );
    window1.addSubplot( v_z,"verlocity z" );

    GnuplotWindow window3( PLOT_AT_EACH_ITERATION );
    window3.addSubplot( roll_cmd,"roll cmd" );
    window3.addSubplot( pitch_cmd,"pitch cmd" );
    window3.addSubplot( yawrate_cmd,"yawrate cmd" );
    window3.addSubplot( T,"Thrust" );


    // Define an algorithm to solve it.
    OptimizationAlgorithm algorithm(ocp);
    algorithm.set( INTEGRATOR_TOLERANCE, 1e-6 );
    algorithm.set( KKT_TOLERANCE, 1e-3 );
    algorithm << window1;
    algorithm << window3;
    algorithm.solve();

  } else {
    // For code generation, we can set some properties.
    // The main reason for a setting is given as comment.
    OCPexport mpc(ocp);

//    mpc.set(HESSIAN_APPROXIMATION,  GAUSS_NEWTON);        // is robust, stable
//    mpc.set(DISCRETIZATION_TYPE,    MULTIPLE_SHOOTING);   // good convergence
//    mpc.set(SPARSE_QP_SOLUTION,     FULL_CONDENSING_N2);  // due to qpOASES
//    mpc.set(INTEGRATOR_TYPE,        INT_IRK_GL4);         // accurate
//    mpc.set(NUM_INTEGRATOR_STEPS,   N);
//    mpc.set(QP_SOLVER,              QP_QPOASES);          // free, source code
//    mpc.set(HOTSTART_QP,            NO);
//    mpc.set(CG_USE_OPENMP,                    YES);       // paralellization
//    mpc.set(CG_HARDCODE_CONSTRAINT_VALUES,    NO);        // set on runtime
//    mpc.set(CG_USE_VARIABLE_WEIGHTING_MATRIX, NO);       // time-varying costs
//    mpc.set( USE_SINGLE_PRECISION,        YES);           // Single precision
//
//    // Do not generate tests, makes or matlab-related interfaces.
//    mpc.set( GENERATE_TEST_FILE,          YES);
//    mpc.set( GENERATE_MAKE_FILE,          NO);
//    mpc.set( GENERATE_MATLAB_INTERFACE,   NO);
//    mpc.set( GENERATE_SIMULINK_INTERFACE, NO);
//
//    // Finally, export everything.
//    if(mpc.exportCode("quadrotor_mpc_codegen") != SUCCESSFUL_RETURN)
//      exit( EXIT_FAILURE );
//    mpc.printDimensionsQP( );

    mpc.set( HESSIAN_APPROXIMATION, GAUSS_NEWTON);
    mpc.set( DISCRETIZATION_TYPE, MULTIPLE_SHOOTING);
    mpc.set( SPARSE_QP_SOLUTION, FULL_CONDENSING_N2); //FULL_CONDENsinG_N2
    mpc.set( INTEGRATOR_TYPE, INT_IRK_GL4);
//    mpc.set( NUM_INTEGRATOR_STEPS, N);
    mpc.set( QP_SOLVER, QP_QPOASES);
    mpc.set( HOTSTART_QP, NO);
    mpc.set( LEVENBERG_MARQUARDT, 1e-10);
    mpc.set( LINEAR_ALGEBRA_SOLVER, GAUSS_LU);
    mpc.set( IMPLICIT_INTEGRATOR_NUM_ITS, 2);
    mpc.set( CG_USE_OPENMP, YES);
    mpc.set( CG_HARDCODE_CONSTRAINT_VALUES, NO);
    mpc.set( CG_USE_VARIABLE_WEIGHTING_MATRIX, NO);

    if (mpc.exportCode( "quadrotor_mpc_codegen" ) != SUCCESSFUL_RETURN)
      exit( EXIT_FAILURE );

    mpc.printDimensionsQP();
  }

  return EXIT_SUCCESS;
}
